from zapv2 import ZAPv2
import os, json

# def getMessages(proxyFile = 'proxy.json', url=''):
def getMessages(proxyDict, url=''):
    #Check proxy config file
    # fileName = ''
    # if proxyFile.startswith('.') or proxyFile.startswith('/'):
    #     fileName = proxyFile
    # else:
    #     fileName = str('.' + '/' + proxyFile)
    #
    # getProxy = dict()
    # if os.path.isfile(fileName):
    #     #get the first proxy in file
    #     fd = open(fileName, 'r')
    #     if fd:
    #         proxies = json.load(fd)
    #         getProxy = proxies[0]
    # else:
    #     print("There is no proxy config file:", proxyFile)
    #     raise SystemExit
    if len(proxyDict) ==0:
        return []

    connectedProxy = dict()
    # connectedProxy['http'] = str("http://" + getProxy["Address"] + ":" + str(getProxy["Port"]))
    # connectedProxy['https'] = str("https://" + getProxy["Address"] + ":" + str(getProxy["Port"]))
    connectedProxy['http'] = str("http://" + proxyDict["Address"] + ":" + str(proxyDict["Port"]))
    connectedProxy['https'] = str("https://" + proxyDict["Address"] + ":" + str(proxyDict["Port"]))
    apiKey = ''
    if 'apikey' in proxyDict:
        apiKey = proxyDict['apikey']

    zap = ZAPv2(proxies=connectedProxy, apikey=apiKey)
    messages = zap.core.messages(baseurl=url)

    return messages

# def getLatestMessage(proxyFile = 'proxy.json', url='', method=''):
def getLatestMessage(proxyDict, url='', method=''):
    allMessages = getMessages(proxyDict = proxyDict, url=url)
    if allMessages.__len__() ==0:
        return ''
    index = 0
    id = int(allMessages[index]["id"])

    if method =='':
        for i in range(allMessages.__len__()):
            if id < int(allMessages[i]["id"]):
                id = int(allMessages[i]["id"])
                index=i
        return allMessages[index]
    else:
        for i in range(allMessages.__len__()):
            if str(allMessages[i]["requestHeader"]).startswith(method):
                index = i
                id = int(allMessages[index]["id"])
                break
        for i in range(index+1, allMessages.__len__()):
            if str(allMessages[i]["requestHeader"]).startswith(method):
                if id < int(allMessages[i]["id"]):
                    id = int(allMessages[i]["id"])
                    index = i
        return allMessages[index]