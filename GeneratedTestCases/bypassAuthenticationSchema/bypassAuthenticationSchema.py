import sys, datetime
from common.httpTester import HTTPTester
from common.system import System

class bypassAuthenticationSchema(HTTPTester):
	def __init__(self, path="./", cookie=""):
		HTTPTester.__init__(self, path, cookie)
		self.loadComplexInput("complexInput.json")

	def run(self):
		print("Check bypassAuthenticationSchema")
		print("---------------------------------")
		startTime = datetime.datetime.now()

		system = System(path=self.rootPath)
		maliciousUser = self
		input = self.complexInput

		resourceIter = input["resource"].__iter__()
		while True: 
			try: 
				resource = resourceIter.__next__()
				if not eval(resource["the_resource_contains_the_role_parameter_in_the_URL"]) :
					if not eval(resource["the_resource_contains_the_role_parameter_in_the_HTTP_post_data"]) :
						system.request(resource)
						maliciousUser.responsePage = system.responsePage
						parameter = dict()
						parameter["error_message"] = resource["error_message"]
						if not maliciousUser.responsePage.contain(parameter) :
							parameters = dict()
							parameters["resource"] = resource["resource"]
							system.exploit(parameters)
						else:
							maliciousUser.abort("The MALICIOUS user CANNOT execute a function dedicated to an authenticated user")
					else:
						parameters1 = dict()
						parameters1["role_values"] = resource["role_values"]
						system.modify(False,True,parameters1)
						system.request(resource)
						maliciousUser.responsePage = system.responsePage
						parameter1 = dict()
						parameter1["error_message"] = resource["error_message"]
						if not maliciousUser.responsePage.contain(parameter1) :
							parameters2 = dict()
							parameters2["resource"] = resource["resource"]
							system.exploit(parameters2)
						else:
							maliciousUser.abort("The MALICIOUS user CANNOT execute a function dedicated to an authenticated user")
				else:
					parameters3 = dict()
					parameters3["role_values"] = resource["role_values"]
					system.modify(True,False,parameters3)
					system.request(resource)
					maliciousUser.responsePage = system.responsePage
					parameter2 = dict()
					parameter2["error_message"] = resource["error_message"]
					if not maliciousUser.responsePage.contain(parameter2) :
						parameters4 = dict()
						parameters4["resource"] = resource["resource"]
						system.exploit(parameters4)
					else:
						maliciousUser.abort("The MALICIOUS user CANNOT execute a function dedicated to an authenticated user")
			except StopIteration: 
				break
		maliciousUser.exit("The MALICIOUS user has executed a function dedicated to an authenticated user")

		print("---------------------------------")
		endTime = datetime.datetime.now()
		print("Tested time:", endTime - startTime)

#----- Running code: bypassAuthenticationSchema -----#
path = "./"
if sys.argv.__len__()>2:
	path = sys.argv[1]

threads = []
bypassAuthenticationSchemaTester = bypassAuthenticationSchema(path)
bypassAuthenticationSchemaTester.start()
threads.append(bypassAuthenticationSchemaTester)
# Wait for all threads to finish #
for thread in threads:
	thread.join()
sys.exit(bypassAuthenticationSchemaTester.STATUS)
