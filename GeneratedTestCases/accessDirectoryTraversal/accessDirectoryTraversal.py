import sys, datetime
from common.httpTester import HTTPTester
from common.system import System

class accessDirectoryTraversal(HTTPTester):
	def __init__(self, path="./", cookie=""):
		HTTPTester.__init__(self, path, cookie)
		self.loadComplexInput("complexInput.json")

	def run(self):
		print("Check accessDirectoryTraversal")
		print("---------------------------------")
		startTime = datetime.datetime.now()

		system = System(path=self.rootPath)
		maliciousUser = self
		input = self.complexInput

		urlIter = input["url"].__iter__()
		while True: 
			try: 
				url = urlIter.__next__()
				while True:
					system.sendFuzzValuesThroughUrl(url)
					maliciousUser.responsePage = system.responsePage
					parameter = dict()
					parameter["error_message"] = url["error_message"]
					if not maliciousUser.responsePage.contain(parameter) :
						parameters = dict()
						parameters["url"] = url["url"]
						system.exploit(parameters)
						if maliciousUser.usedAllFuzzValues() :
							break
					else:
						if maliciousUser.usedAllFuzzValues() :
							break
			except StopIteration: 
				break
		maliciousUser.exit("The MALICIOUS user may have accessed sensitive files or folders on the Web server")

		print("---------------------------------")
		endTime = datetime.datetime.now()
		print("Tested time:", endTime - startTime)

#----- Running code: accessDirectoryTraversal -----#
path = "./"
if sys.argv.__len__()>2:
	path = sys.argv[1]

threads = []
accessDirectoryTraversalTester = accessDirectoryTraversal(path)
accessDirectoryTraversalTester.start()
threads.append(accessDirectoryTraversalTester)
# Wait for all threads to finish #
for thread in threads:
	thread.join()
sys.exit(accessDirectoryTraversalTester.STATUS)
