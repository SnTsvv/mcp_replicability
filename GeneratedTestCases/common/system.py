import pycurl
import sys
import tempfile
import os
import json

from tools.tool import *
from common.httpTester import HTTPTester
from common.resource import *

class System(HTTPTester):

    def __init__(self, path: str = "./TestCases/", cookie:str ="") -> None:
        HTTPTester.__init__(self, path=path, cookie=cookie)

    def send(self, page:str, parameters: dict) -> None:
        """
        Send a request to access a page from the system.
        :param page: the name of page
        :type page: str
        :param parameters: the parameters which will be included in HTTP packet
        :type parameters: dict
        :returns: response page in self.system.responsePage, and the cookie (if login)
        :rtype: None
        """
        # 1. get param from channel (page)
        # self.loadHTTPHeaderTemplate(self.httpHeaderTemplateFile)
        channelFilePath = str(page)
        # erase the article at the beginning
        # if channelFilePath.startswith("the "):
        #     channelFilePath = channelFilePath[4:]
        # if channelFilePath.startswith("a ") | channelFilePath.startswith("A "):
        #     channelFilePath = channelFilePath[2:]
        # if channelFilePath.startswith("an ") | channelFilePath.startswith("An ") \
        #         | channelFilePath.startswith("AN "):
        #     channelFilePath = channelFilePath[3:]
        channelFilePath = eraseArticle(channelFilePath)

        if str(channelFilePath).upper().endswith("PAGE"):
            channelFilePath = channelFilePath[:channelFilePath.__len__()-4].strip().replace(' ', '_')
        channelFilePath = self.rootPath + channelFilePath + "Params.json"
        if os.path.exists(channelFilePath) == False:
            return None

        # Get parameters from channel file
        pageURL, paramsPosition, pageParams = self.getPageParamsFromFile(channelFilePath)

        # Get parameters from  page
        sessionParams = pageParams.copy()
        sessionIDs = self.getSessionID(pageURL, sessionParams)  # get session id from page

        '''
        # 2. check where do dictParams exist? complexInput, args.txt?
        #2.1 check if the complexInput.json file exists
        complexInputFile = str(self.rootPath + self.COMPLEX_INPUT_FILE).strip()
        if os.path.exists(complexInputFile):
            if self.complexInput.__len__()<=0:
                #load complexInput
                self.loadComplexInput(self.COMPLEX_INPUT_FILE)
            #2.2 Check if self.complexInput contains parameters (indicated in dictParams)
            # TODO
            print("")

        #2.2 check if an independent input file exists
        else:
            inputFile = ""
            #TODO

        #3. Set ID
        '''

        #Update sessionIDs by using dictParams
        # sessionIDs.update(parameters)
        updateParameters = dict()
        for key in parameters.keys():
            if isinstance(parameters[key], dict):
                for subKey in dict(parameters[key]).keys():
                    updateParameters[subKey] = parameters[key][subKey]
            else:
                updateParameters[key] = parameters[key]
        sessionIDs = self.updatePageParametersValues(channelFilePath, sessionIDs, updateParameters)


        #4. Config pycurl before requesting login
        self.setHTTPHeaders(pageURL)
        if paramsPosition == "BOTH":
            self.setParamsInURL(pageURL, sessionIDs)
            self.setParamsInPostData(sessionIDs)
        elif paramsPosition == "URL":
            self.setParamsInURL(pageURL, sessionIDs)
        else:
            self.setParamsInPostData(sessionIDs)

        oldCookie = self.cookie
        if str(page).upper().replace(' ', '').endswith("LOGINPAGE")\
                |str(page).upper().replace(' ', '').endswith("LOGIN"):
            # Remove cookie before trying to log in
            HTTPTester.curl.setopt(pycurl.COOKIELIST, "ALL")

        #5. Request the page
        HTTPTester.curl.setopt(pycurl.HEADERFUNCTION, self.getResponseHeadersCallback)
        self.gotFile = tempfile.NamedTemporaryFile()
        HTTPTester.curl.setopt(pycurl.WRITEDATA, self.gotFile)
        try:
            HTTPTester.curl.perform()
            # Reset POST-Data
            HTTPTester.curl.setopt(pycurl.POSTFIELDS, "")

            self.gotFile.seek(0)
            # update self.system.responsePage
            body = self.gotFile.read()
            responseCode = HTTPTester.curl.getinfo(pycurl.HTTP_CODE)
            # self.system.loadResponsePage(body.decode(self.responseEncode), responseCode=responseCode)
            self.loadResponsePage(body.decode(self.responseEncode), responseCode=responseCode)
            self.gotFile.seek(0)
        except:
            import traceback
            traceback.print_exc(file=sys.stderr)
            sys.stderr.flush()

        if str(page).upper().replace(' ', '').endswith("LOGINPAGE") \
                | str(page).upper().replace(' ', '').endswith("LOGIN"):
            newCookie = self.cookie
            if (oldCookie != newCookie) & (newCookie!=''):
                self.loggedIn = True

    def setParamsInURL(self, url:str, parameters:dict ={}) -> bool:
        """
        Set params in URL
        :param url: URL to set
        :type url: str
        :param parameters: parameters under dictionary format to set, e.g., {"role":"Admin","login":"yes"...}
        :type parameters: dict
        :return: 0 if could not set role, 1 if have set role
        :rtype: bool
        """

        haveSet = False  # return value
        if len(parameters) == 0:
            return haveSet
        # self.lastMethod = "GET"
        HTTPTester.lastRequest.method = "GET"

        # Get parts and current queries in the URL
        urlParts = list(urlparse.urlparse(url))
        urlQuery = dict(urlparse.parse_qsl(urlParts[4]))
        addParams = dict()
        for param in parameters:
            if isinstance(parameters[param], dict):
                for key in parameters[param]:
                    addParams[key] = parameters[param][key]
            else:
                addParams[param] = parameters[param]

        #1. Replace value in the cases:
        #a. "key" (in parameters) is a part of scheme (0), netloc (1), path (2)
        #b. "key" (in parameters) is a part of query (4) value
        removeKey = []
        if len(addParams)>0:
            for key in addParams.keys():
                # a. scheme (0)
                if urlParts[0] == key:
                    urlParts[0] = addParams[key]
                    removeKey.append(key)
                # a. netloc (1)
                elif str(urlParts[1]).find(key) >=0:
                    urlParts[1] = str(urlParts[1]).replace(key, addParams[key])
                    removeKey.append(key)
                # a. path(2)
                elif str(urlParts[2]).find(key) >= 0:
                    urlParts[2] = str(urlParts[2]).replace(key, addParams[key])
                    removeKey.append(key)
                # b. query value
                else:
                    for queryKey in urlQuery:
                        if urlQuery[queryKey] == key:
                            urlQuery[queryKey] = addParams[key]
                            removeKey.append(key)
        # update addParams
        if removeKey.__len__() >0:
            haveSet = True
            for i in range(removeKey.__len__()):
                addParams.pop(removeKey[i], None)

        #2. Update parameters in URL
        #("key" of parameters is a key in query (4))


        if len(addParams) >0:
            urlQuery.update(addParams)
            haveSet = True

        if haveSet:
            urlParts[4] = urlencode(urlQuery)

            newURL = urlunparse(urlParts)
            # HTTPTester.curl.setopt(pycurl.URL, newURL)
            self.setHTTPHeaders(newURL)
            # self.lastURL = newURL
            HTTPTester.lastRequest.url = newURL

        return haveSet

    def setParamsInPostData(self, parameters:dict={}) -> bool:
        """
        Set params in POST Data
        :param parameters: parameters under dictionary format to set, e.g., {"role":"Admin","login":"yes"...}
        :type parameters: dict
        :return: 0 if could not set role, 1 if have set role
        :rtype: bool
        """
        haveSet = 0  # return value: 0 if could not set role, 1 if have set role
        if len(parameters) > 0:
            # self.lastMethod = "POST"
            HTTPTester.lastRequest.method = "POST"
            HTTPTester.curl.setopt(pycurl.POST, 1)
            # Add Post Data
            postData = dict()
            # Get Post-Data from HTTP Headers template
            if "POST-Data" in self.httpTemplate:
                for element in self.httpTemplate["POST-Data"]:
                    postData[element] = self.httpTemplate["POST-Data"][element]
            # Get Post-Data from HTTPTester.lastRequest.postData
            if len(HTTPTester.lastRequest.postData) >0:
                postData.update(HTTPTester.lastRequest.postData)

            # Add params into Post-Data
            for param in parameters:
                if isinstance(parameters[param], dict):
                    for key in parameters[param]:
                        postData[key] = parameters[param][key]
                else:
                    postData[param] = parameters[param]
            if len(postData) > 0:
                encodedPostData = urlencode(postData)
                HTTPTester.curl.setopt(pycurl.POSTFIELDS, encodedPostData)
                HTTPTester.lastRequest.postData = postData.copy()
                haveSet = 1
        return haveSet

    def request(self, resource: dict = None) -> None:
        """
        Send a request to the system, and get response page
        :param resource: (an object of Resource) indicate a resource to request
        :type resource: Resource
        :return: response page (and response code) will be store in self.responsePage
        :rtype: None
        """
        HTTPTester.curl.setopt(pycurl.HEADERFUNCTION, self.getResponseHeadersCallback)
        self.gotFile = tempfile.NamedTemporaryFile()
        HTTPTester.curl.setopt(pycurl.WRITEDATA, self.gotFile)
        if resource != None:
            if isinstance(resource, Resource):
                self.setHTTPHeaders(resource.get("url").getURL())
                if not resource.get("postData").isnull():
                    self.setParamsInPostData(resource.get("postData"))
            elif isinstance(resource, dict):
                for key in resource.keys():
                    if (str(key).lower() == "url") or (str(key).lower() == "resource"):
                        self.setHTTPHeaders(resource[key])
                    elif str(key).lower() == "postdata":
                        if len(resource[key]) != 0:
                            self.setParamsInPostData(resource[key])
        else:
            # update HTTP request info from HTTPTester.lastRequest
            self.updateConfigFromLastRequest()
        # HTTPTester.curl.setopt(pycurl.HEADERFUNCTION, self.getResponseHeadersCallback)
        # self.gotFile = tempfile.NamedTemporaryFile()
        # HTTPTester.curl.setopt(pycurl.WRITEDATA, self.gotFile)
        # TODO: load lastRequest
        try:
            HTTPTester.curl.perform()
            # Reset POST-Data
            HTTPTester.curl.setopt(pycurl.POSTFIELDS, "")

            self.gotFile.seek(0)
            # update self.system.responsePage
            body = self.gotFile.read()
            responseCode = HTTPTester.curl.getinfo(pycurl.HTTP_CODE)
            # self.system.loadResponsePage(body.decode(self.responseEncode), responseCode=responseCode)
            self.loadResponsePage(body.decode(self.responseEncode), responseCode=responseCode)
            # self.responsePage.sets(self.system.responsePage.getContent(),
            #                       self.system.responsePage.getCode())     #manually use this statement
            self.gotFile.seek(0)
        except:
            import traceback
            traceback.print_exc(file=sys.stderr)
            sys.stderr.flush()
        sys.stdout.flush()

    def getPageParamsFromFile(self, fileName: str) -> tuple:
        """
        Get information and parameters of a page from a page param file
        :param fileName: page param file name
        :type fileName: str
        :return: tuple of url (str), position (str) and parameters (dict)
        :rtype: tuple
        """
        url = ""
        position = ""
        params = []
        if fileName != "":
            try:
                fd = open(fileName, 'r')
                if fd:
                    # Load Roles in JSON format
                    try:
                        gottenParams = json.load(fd)
                        fd.close()

                        for key in gottenParams.keys():
                            # if key == "URL":
                            #     url = gottenParams["URL"]
                            # elif key == "Position":
                            #     position = gottenParams["Position"]
                            #     if position != "BOTH" and position != "URL" and position != "POST-data":
                            #         position = "POST-data"
                            # elif key == "Others":
                            #     for i in range(len(gottenParams["Others"])):
                            #         params.append(gottenParams["Others"][i])
                            # else:
                            #     params.append(gottenParams[key])
                            if not isinstance(gottenParams[key], dict):
                                if key=="URL":
                                    url = gottenParams["URL"]
                                elif key == "Position":
                                    position = gottenParams["Position"]
                                    if position != "BOTH" and position != "URL" and position != "POST-data":
                                        position = "POST-data"
                                elif key == "Others":
                                    for i in range(len(gottenParams["Others"])):
                                        params.append(gottenParams["Others"][i])
                                else:
                                    params.append(gottenParams[key])
                            else:
                                for subKey in gottenParams[key].keys():
                                    params.append(gottenParams[key][subKey])
                        # if len(gottenParams) > 0:
                        #     if "URL" in gottenParams:
                        #         url = gottenParams["URL"]
                        #     if "Position" in gottenParams:
                        #         position = gottenParams["Position"]
                        #         if position != "BOTH" and position != "URL" and position != "POST-data":
                        #             position = "POST-data"
                        #     else:  # default
                        #         position = "POST-data"
                        #     if "Username" in gottenParams:
                        #         params.append(gottenParams["Username"])
                        #     else:  # default
                        #         params.append("username")
                        #     if "Password" in gottenParams:
                        #         params.append(gottenParams["Password"])
                        #     else:  # default
                        #         params.append("password")
                        #     if "Others" in gottenParams:
                        #         for i in range(len(gottenParams["Others"])):
                        #             params.append(gottenParams["Others"][i])
                    except:
                        print("Cannot load params in JSON format")
                        print("The tester continues with default params!")
            except:
                print("Cannot open the login param file: %s" % fileName)
                print("The tester continues with default params!")
        return url, position, params

    def modify(self, url:bool, postData:bool, parameters:dict) -> None:
        """
        Modify paremeters in URL or Post Data
        :param url: if modify URL parameters
        :type url: bool
        :param postData: if modify Post Data parameters
        :type postData: bool
        :param parameters: parameters to set
        :type parameters: dict
        """
        # Update URL parameters
        if url:
            # lastURL = self.lastURL
            lastURL = self.lastRequest.url
            self.setParamsInURL(lastURL, parameters=parameters)
        # Update Post Data parameters
        if postData:
            self.setParamsInPostData(parameters=parameters)

    def resendModifiedPacket(self):
        self.request()

    def sendFuzzValuesThroughUrl(self, url:dict) -> None:
    # Only check FUZZ in URL parameters (queries)
        usedURL = ""
        if isinstance(url, str):
            usedURL = url
        elif isinstance(url, dict):
            for key in url:
                if str(key).lower() == "url":
                    usedURL = url[key]
                    break
                elif str(key).lower() == "resource":
                    usedURL = url[key]
                    break

        if usedURL == "":
            print("Error: sendFuzzValuesToTheSystem function needs a URL!!!")
            exit(0)

        # Get parts and current queries in the URL
        urlParts = list(urlparse.urlparse(usedURL))
        urlQuery = dict(urlparse.parse_qsl(urlParts[4]))

        fuzzList = []

        # 1. get fuzz name from scheme (0)
        if str(urlParts[0]).startswith("FUZZ"):
            fuzzList.append(urlParts[0])
        # 2. get fuzz name from netloc (1)
        for part in str(urlParts[1]).split("."):
            if part.startswith("FUZZ"):
                fuzzList.append(part)
        # 3. get fuzz name from path(2)
        for partPath in str(urlParts[2]).split("/"):
            if partPath.startswith("FUZZ"):
                fuzzList.append(partPath)
        # 4. get fuzz name from query value
        for queryKey in urlQuery:
            if str(urlQuery[queryKey]).startswith("FUZZ"):
                fuzzList.append(str(urlQuery[queryKey]))
        # 5. get fuzz name from queries' key
        for key in urlQuery:
            if str(key).startswith("FUZZ"):
                fuzzList.append(key)

        # If there are no FUZZ parameter --> send the request
        if len(fuzzList) ==0:
            self.request({"url":usedURL})
            return None

        #check if the current fuzzContainer is NOT the correct case
        if not HTTPTester.fuzzContainer.isTheCase(fuzzList):
            HTTPTester.fuzzContainer.setFuzzList(fuzzList)      # set fuzz list
            HTTPTester.fuzzContainer.loadFuzz(self.rootPath)    # load all fuzz values

        fuzzDictToUse = HTTPTester.fuzzContainer.next()

        if fuzzDictToUse != None:
            #Set fuzzValues in URL
            self.setParamsInURL(usedURL, fuzzDictToUse)

            HTTPTester.curl.setopt(pycurl.HEADERFUNCTION, self.getResponseHeadersCallback)
            self.gotFile = tempfile.NamedTemporaryFile()
            HTTPTester.curl.setopt(pycurl.WRITEDATA, self.gotFile)
            try:
                HTTPTester.curl.perform()

                self.gotFile.seek(0)
                body = self.gotFile.read()
                responseCode = HTTPTester.curl.getinfo(pycurl.HTTP_CODE)
                self.loadResponsePage(body.decode(self.responseEncode), responseCode=responseCode)
                self.gotFile.seek(0)
            except:
                import traceback
                traceback.print_exc(file=sys.stderr)
                sys.stderr.flush()
            sys.stdout.flush()

    def assignFuzzValuesToHTTPRequest(self) -> None:
        """
        Set fuzz values to HTTP method, HTTP version, headers values
        :rtype: None
        """
        self.loadHTTPHeaderTemplate(self.httpHeaderTemplateFile)
        fuzzList = []

        for key in self.httpTemplate:
            if str(self.httpTemplate[key]).startswith("FUZZ"):
                fuzzList.append(self.httpTemplate[key])

        # check if the current fuzzContainer is NOT the correct case
        if not HTTPTester.fuzzHTTPRequestContainer.isTheCase(fuzzList):
            HTTPTester.fuzzHTTPRequestContainer.setFuzzList(fuzzList)  # set fuzz list
            HTTPTester.fuzzHTTPRequestContainer.loadFuzz(self.rootPath)  # load all fuzz values

        fuzzDictToUse = HTTPTester.fuzzHTTPRequestContainer.next()

        # check if the current fuzzContainer is NOT the correct case
        # if not HTTPTester.fuzzContainer.isTheCase(fuzzList):
        #     HTTPTester.fuzzContainer.setFuzzList(fuzzList)  # set fuzz list
        #     HTTPTester.fuzzContainer.loadFuzz(self.rootPath)  # load all fuzz values
        #
        # fuzzDictToUse = HTTPTester.fuzzContainer.next()

        if fuzzDictToUse != None:
            for key in self.httpTemplate:
                for fuzzKey in fuzzDictToUse:
                    if self.httpTemplate[key] == fuzzKey:
                        self.httpTemplate[key] = fuzzDictToUse[fuzzKey]
                        break
