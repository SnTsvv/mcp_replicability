import os

class FuzzContainer:
    def __init__(self, fuzzList:list=[]):
        self.counter = 0
        self.fuzzList = fuzzList    # List of names of FUZZ
        self.fuzzValues = []        # List of all tuples of value
        self.iter = self.fuzzValues.__iter__()
        self.visitedAll = False
        self.current = {}

    def next(self) -> dict:
        if self.hasNext():
            # nextValue = self.iter.__next__()
            # self.current = nextValue.copy()
            # return nextValue
            return self.iter.__next__()
        else:
            return None

    def current(self) -> dict:
        return self.current

    def hasNext(self) -> bool:
        if self.iter.__length_hint__() > 0:
            return True
        else:
            self.visitedAll = True
            return False

    def isTheCase(self, fuzzList:list) -> bool:
        '''
        Check if this is the correct case to use
        :param fuzzList:
        :return:
        '''
        if len(self.fuzzList) != len(fuzzList):
            return False

        if not all(element in self.fuzzList for element in fuzzList):
            return False

        # If all values are visited
        if self.visitedAll:
            return False
            #The fuzzContainer should be reloaded

        return True

    def setFuzzList(self, fuzzList:list) -> None:
        if fuzzList != None and fuzzList.__len__()>0:
            self.fuzzList = fuzzList.copy()

    def addFuzzList(self, fuzz:str) -> None:
        if isinstance(fuzz, str) and fuzz.__len__()>0:
            if fuzz not in self.fuzzList:
                self.fuzzList.append(fuzz)
                self.fuzzList.sort()

    def loadFuzz(self, rootPath:str="./") -> None:
        '''
        Load fuzz values from files, then generate tuples
        :return:
        '''
        self.fuzzValues = []        #reset self.fuzzValues
        self.visitedAll = False
        self.fuzzList.sort()
        usedList = []
        for i in range(len(self.fuzzList)):
            filePath = rootPath + self.fuzzList[i] + ".txt"
            if os.path.exists(filePath):
                usedList.append(self.fuzzList[i])
            else:
                print(filePath, "does not exist!")

        fuzzValuesDict = dict()     #used to store list of values of each fuzz
        if len(usedList) >0:
            usedList.sort()

            #load all fuzz files
            for file in usedList:
                filePath = rootPath + file + ".txt"
                try:
                    fd = open(filePath, "r")
                    if fd:
                        try:
                            lines = fd.readlines()
                            fd.close()
                            valuesList = []
                            for line in lines:
                                if line.strip()[0] != "#":
                                    valuesList.append(line.strip())
                            fuzzValuesDict[file] = valuesList
                        except:
                            print("Cannot load", filePath + "!")
                except:
                    print("Cannot open", filePath + "!")

            #Mix all fuzz values
            perString1 = ""
            perString2 = ""
            for i in range(len(usedList)):
                if i == 0:
                    perString1 = "{\"" + usedList[i] + "\":" + usedList[i]+ "_ele}"
                    perString2 = "for " + usedList[i]+ "_ele in fuzzValuesDict[\"" + usedList[i] + "\"]"
                else:
                    perString1 += ", {\"" + usedList[i] + "\":" + usedList[i] + "_ele}"
                    perString2 += " for " + usedList[i]+ "_ele in fuzzValuesDict[\"" + usedList[i] + "\"]"

            permutationString = "[[" + perString1 + "] " + perString2 + "]"
            scope = locals()
            permutations = eval(permutationString, scope)

            for element in permutations:
                subDict = dict()
                for se in element:
                    subDict.update(se)
                self.fuzzValues.append(subDict)

        #Update iterator
        self.iter = self.fuzzValues.__iter__()