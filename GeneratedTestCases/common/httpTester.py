import json
import os
import pycurl
import re
import tempfile
import threading
import certifi
import sys
import urllib.parse as urlparse
from urllib.parse import urlsplit, urlencode, urlunparse

from common.responsePage import ResponsePage
from common.resource import Resource
from common.requestInfo import RequestInfo
from common.fuzzContainer import FuzzContainer
from tools.tool import *
from tools import zapProxy

# We should ignore SIGPIPE when using pycurl.
try:
    import signal
    from signal import SIGPIPE, SIG_IGN
except ImportError:
    pass
else:
    signal.signal(SIGPIPE, SIG_IGN)


class HTTPTester(threading.Thread):
    # Define some constant strings related to file names using in the HTTPTester
    HTTP_HEADER = "headersTemplate.json"
    COOKIE_FILE = "cookie.txt"
    COMPLEX_INPUT_FILE = "complexInput.json"
    TEST_PASS = 0
    TEST_FAIL = 1
    curl = pycurl.Curl()
    lastRequest = RequestInfo()
    fuzzContainer = FuzzContainer()
    fuzzHTTPRequestContainer = FuzzContainer()
    STATUS = TEST_PASS  # status of test (0: passed, more than 0: failed)

    def __init__(self, path: str = "./Test cases/", cookie:str ="") -> None:
        """
        :parameter path: path to the folder containing all test inputs and configurations
        :type path: str
        :parameter cookie: cookie value
        :type cookie: str
        :rtype: None
        """
        threading.Thread.__init__(self)
        # File names

        self.attemptCounter = 0
        self.rootPath = path.strip()
        if len(self.rootPath)>0 and self.rootPath[len(self.rootPath) - 1] != '/':
            self.rootPath = self.rootPath + '/'

        self.httpHeaderTemplateFile = str(self.rootPath + self.HTTP_HEADER)

        self.lastURL = ""       # the URL just visited
        self.lastMethod = ""    # the HTTP Method just used (GET, POST...)

        self.httpTemplate = dict()  # HTTP headers values  --> call loadHTTPHeaderTemplate
        self.proxy = []             # list of Proxies      --> call loadProxy, getRandomProxy
        self.usedProxy = dict()     # used proxy           --> updated by getRandomProxy
        self.complexInput = dict()  # complex input        --> call loadComplexInput

        # Temporary file which contains current responded page
        self.gotFile = tempfile.NamedTemporaryFile()  # raw response page
        self.responsePage = ResponsePage(path=self.rootPath)  # decoded response page
        HTTPTester.curl.setopt(pycurl.WRITEDATA, self.gotFile)
        self.setupCurl()
        self.loadHTTPHeaderTemplate(self.httpHeaderTemplateFile)
        self.cookie = ''
        self.setCookie(cookie)
        self.loggedIn = False
        HTTPTester.curl.setopt(pycurl.COOKIEJAR, self.COOKIE_FILE)
        # Type of used encode for response
        self.responseEncode = 'iso-8859-1'  # by default
        # Load and set (randomly) proxy
        self.loadProxy()
        self.getRandomProxy()

    def loadHTTPHeaderTemplate(self, headerFileName: str) -> None:
        """
        :type headerFileName: str
        :rtype: None
        """
        try:
            fd = open(headerFileName, 'r')
            if fd:
                # Load HTTP Headers template in JSON format
                try:
                    self.httpTemplate = json.load(fd)
                    fd.close()
                except:
                    print("Cannot load the HTTP headers template in JSON format")
                    print("The HTTPTester uses the default HTTP headers of pycurl!")
        except:
            print("Cannot open the HTTP headers template file: %s" % headerFileName)
            print("The HTTPTester uses the default HTTP headers of pycurl!")

    def setupCurl(self, verbose:bool =False, proxy:str ="NONE") -> None:
        """

        :param verbose:
        :type verbose: bool
        :param proxy:
        :type proxy: str
        :rtype: None
        """
        HTTPTester.curl.setopt(pycurl.FOLLOWLOCATION, 1)
        HTTPTester.curl.setopt(pycurl.MAXREDIRS, 5)
        HTTPTester.curl.setopt(pycurl.CONNECTTIMEOUT, 30)
        HTTPTester.curl.setopt(pycurl.TIMEOUT, 300)
        HTTPTester.curl.setopt(pycurl.NOSIGNAL, 1)
        HTTPTester.curl.setopt(pycurl.VERBOSE, verbose)
        HTTPTester.curl.setopt(pycurl.HEADERFUNCTION, self.getResponseHeadersCallback)
        if proxy != "NONE":
            HTTPTester.curl.setopt(pycurl.PROXY, proxy)

    def loadProxy(self, proxyFileName:str ="proxy.json") -> None:
        """
        Load proxies information from json file
        :param proxyFileName:
        :type proxyFileName: str
        :rtype: None
        """
        fileName = self.rootPath + proxyFileName
        if os.path.isfile(fileName):
            fd = open(fileName, 'r')
            if fd:
                self.proxy = json.load(fd)
                # else:
                #     print("There is no proxy config file:", proxyFile)

    def getRandomProxy(self) -> dict:
        """
        :return: a proxy, and configure pycurl with proxy information
        :rtype: dict
        """
        numProxies = self.proxy.__len__()
        if numProxies == 0:
            # There is no proxy to set
            HTTPTester.curl.setopt(pycurl.CAINFO, certifi.where())
            # return False
            return dict()
        index = random.randint(0, numProxies - 1)
        # Set proxy to curl
        HTTPTester.curl.setopt(pycurl.PROXY, self.proxy[index]["Address"])
        if "Port" in self.proxy[index]:
            if self.proxy[index]["Port"] != "":
                HTTPTester.curl.setopt(pycurl.PROXYPORT, self.proxy[index]["Port"])
        if "Username" in self.proxy[index] and "Password" in self.proxy[index]:
            if self.proxy[index]["Username"] != "":
                HTTPTester.curl.setopt(pycurl.PROXYUSERNAME, self.proxy[index]["Username"])
                HTTPTester.curl.setopt(pycurl.PROXYPASSWORD, self.proxy[index]["Password"])

        HTTPTester.curl.setopt(pycurl.SSL_VERIFYPEER, 1)
        HTTPTester.curl.setopt(pycurl.SSL_VERIFYHOST, 2)
        if "Certificate" in self.proxy[index]:
            if self.proxy[index]["Certificate"] != "":
                certFile = str(self.rootPath + self.proxy[index]["Certificate"])
                if os.path.isfile(certFile):
                    HTTPTester.curl.setopt(pycurl.CAINFO, certFile)
        else:
            HTTPTester.curl.setopt(pycurl.CAINFO, certifi.where())
        # return True
        self.usedProxy = self.proxy[index]
        return self.proxy[index]

    def setHTTPHeaders(self, url:str='') -> None:
        """
        The method configures the HTTP Headers based on a template described in JSON format in a file
        :param url: URL
        :type url: str
        """
        # urlParts = list(urlparse.urlparse(url))
        headers = []
        if url != '':
            HTTPTester.curl.setopt(pycurl.URL, url)
            # self.lastURL = url
            HTTPTester.lastRequest.url = url

        if len(self.httpTemplate) > 0:
            for field in self.httpTemplate:
                if field == "Method":
                    # self.lastMethod = str(self.httpTemplate["Method"]).strip()
                    self.lastRequest.method = str(self.httpTemplate["Method"]).strip()
                    if self.httpTemplate["Method"] == "GET":
                        HTTPTester.curl.setopt(pycurl.HTTPGET, 1)
                    elif self.httpTemplate["Method"] == "PUT":
                        HTTPTester.curl.setopt(pycurl.PUT, 1)
                    elif self.httpTemplate["Method"] == "HEAD":
                        HTTPTester.curl.setopt(pycurl.NOBODY, 1)
                    elif self.httpTemplate["Method"] == "POST":
                        HTTPTester.curl.setopt(pycurl.POST, 1)
                        # Add Post Data
                        postData = dict()
                        if "POST-Data" in self.httpTemplate:
                            for element in self.httpTemplate["POST-Data"]:
                                postData[element] = self.httpTemplate["POST-Data"][element]
                        if len(postData) > 0:
                            HTTPTester.lastRequest.postData = postData.copy()
                            encodedPostData = urlencode(postData)
                            HTTPTester.curl.setopt(pycurl.COPYPOSTFIELDS, encodedPostData)
                    else:
                        HTTPTester.curl.setopt(pycurl.CUSTOMREQUEST, self.httpTemplate["Method"])
                elif field == "Version":
                    HTTPTester.lastRequest.httpVersion = self.httpTemplate[field]
                    if self.httpTemplate[field] == "1.0":
                        HTTPTester.curl.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_1_0)
                    elif self.httpTemplate[field] == "1.1":
                        HTTPTester.curl.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_1_1)
                    elif self.httpTemplate[field] == "2.0":
                        HTTPTester.curl.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_2_0)
                # elif field == "User-Agent":
                #     HTTPTester.curl.setopt(pycurl.USERAGENT, self.httpTemplate[field])
                elif field != "POST-Data" and field != "Host":
                    headers.append('%s: %s' % (str(field), str(self.httpTemplate[field])))
            # Set default HTTP Method (GET)
            if not "Method" in self.httpTemplate.keys():
                # self.lastMethod = "GET"
                self.lastRequest.method = "GET"
                HTTPTester.curl.setopt(pycurl.HTTPGET, 1)
        # Set Hostname
        hostName = "{0.netloc}".format(urlsplit(url))
        headers.append('Host: %s' % hostName)
        HTTPTester.curl.setopt(pycurl.HTTPHEADER, headers)
        HTTPTester.lastRequest.headers = headers.copy()

    def loadComplexInput(self, fileName:str) -> None:
        """
        Load the complex input from file
        :param fileName:
        :type fileName: str
        :rtype: None
        """
        complexInputFile = str(self.rootPath + fileName).strip()
        if os.path.exists(complexInputFile):
            try:
                fd = open(complexInputFile, 'r')
                if fd:
                    # Load Roles in JSON format
                    try:
                        complexInput = json.load(fd)
                        fd.close()
                        self.complexInput = complexInput
                    except:
                        print("Cannot load complex input in JSON format")
            except:
                print("Cannot open the input file: %s" % complexInputFile)
        else:
            print("You do NOT have the input file (%s)!" % complexInputFile)

    def setCookie(self, cookie: str) -> None:
        """ DO NOT USE
        The method sets cookie value to headers list.
        This function should be called in the setHTTPHeaders method, after loading params from HTTP Headers template
        :rtype: None
        :param cookie: cookie value to set
        :type cookie: str
        """
        if cookie != '':
            self.cookie = cookie
            HTTPTester.curl.setopt(pycurl.COOKIE, cookie)

    def getResponseHeadersCallback(self, header_line:str) -> None:
        """
        This function will get the cookie, the response encode from HTTP Response Headers, and update self.cookie
        :param header_line:
        :type header_line: str
        :return: self.cookie, self. responseEncode
        :rtype: None
        """
        header_line = header_line.decode('iso-8859-1')
        if ':' not in header_line:
            return
        name, value = header_line.split(':', 1)
        name = name.strip()
        value = value.strip()
        if name == "Set-Cookie":
            # self.cookie = str(value.split(';', 1)[0])
            self.cookie = str(value)
        haveEncode = False
        if name == "Content-Type":
            allParams = value.split(';', 1)
            for param in allParams:
                if param.strip().startswith('charset='):
                    encode = str(param).strip().replace('charset=', '')
                    self.responseEncode = encode
                    haveEncode = True
        if haveEncode == False:
            self.responseEncode = 'iso-8859-1'

    def getSessionID(self, url:str, parameters:list) -> dict:
        """
        Get parameters from a the web page
        :param url: URL
        :type url: str
        :param parameters: List of parameters (string type)
        :type parameters: list
        :return: dictionary containing params and their values
        :rtype: dict
        """
        result = dict()
        if parameters.__len__() <= 0:
            return result
        # Get page from url
        self.setHTTPHeaders(url)
        HTTPTester.curl.setopt(pycurl.HTTPGET, 1)
        self.gotFile = tempfile.NamedTemporaryFile()
        HTTPTester.curl.setopt(pycurl.WRITEDATA, self.gotFile)
        HTTPTester.curl.setopt(pycurl.HEADERFUNCTION, self.getResponseHeadersCallback)
        HTTPTester.curl.perform()
        self.gotFile.seek(0)
        body = self.gotFile.read()
        # Get each param value from the gotten page
        for param in parameters:
            # Using RegEx to find param in the body: name="param" value="valueToGet"
            pattern = str("(?<=name=\"%s\" value=\")[^\"]*" % param)
            p = re.compile(pattern)
            # value = p.findall(str(body.decode('iso-8859-1')))
            value = p.findall(str(body.decode(self.responseEncode)))
            if value.__len__() >= 1:
                result[param] = value[0]
            elif value.__len__() <= 0:
                result[param] = ''
            if value.__len__() >= 2:
                print("The parameter %s has many values!" % param)
        return result

    def generateHTTPTemplate(self, requestHeader:str, requestBody:str='') -> None:
        """
        Generate HTTP template, then store new template in the file
        :param requestHeader:
        :type requestHeader: str
        :param requestBody:
        :type requestBody: str
        :rtype: None
        :return: new HTTP template (in file)
        """
        template = dict()
        listHeaders = str(requestHeader).splitlines()
        # First line: Request line = "METHOD URL HTTP_Version"
        requestLine = str(listHeaders[0]).split()
        template["Method"] = requestLine[0]
        template["Version"] = requestLine[2].replace("HTTP/", "")
        # Other lines: headers
        for i in range(1, listHeaders.__len__()):
            header = listHeaders[i].split(':', 1)
            # header[0]: Header name
            # header[1]: Header value
            if header[0] != '' and header[0] != "Content-Length":
                template[header[0]] = str(header[1]).strip()
        # TODO: take care the case params are in URL
        # Add POST-Data
        if template["Method"] == "POST" and requestBody != '':
            template["POST-Data"] = dict(urlparse.parse_qsl(requestBody))
        fd = open(self.httpHeaderTemplateFile, "w")
        fd.write(str(template).replace('\'', '\"'))
        fd.close()

    def loadInputFromFile(self, fileName: str) -> list:
        """
        Load inputs from a file
        :param fileName:
        :type fileName: str
        :return: input values
        :rtype: list
        """
        result = []
        try:
            fd = open(fileName, 'r')
            if fd:
                try:
                    lines = fd.readlines()
                    fd.close()
                    if lines.__len__() < 2:
                        print(
                            "The input file %s requires at least 3 lines: description, template and input value lines" % fileName)
                        return
                    dirPath, fileName = os.path.split(fileName)
                    descriptionLine = fileName + ": Template for INPUT"
                    # determine list of variables
                    startLine = 0
                    variables = []
                    if lines[0].find(descriptionLine) >= 0:
                        if lines[1][0] == '#':
                            templateLine = lines[1].strip().strip('#').replace(' ', '')
                            variables = templateLine.split(',')
                            startLine = 2
                        else:
                            print("The input file %s does not contain template line: #var1, var2,..." % fileName)
                            return
                    else:
                        if lines[0][0] == '#':
                            templateLine = lines[0].strip().strip('#').replace(' ', '')
                            variables = templateLine.split(',')
                            startLine = 1
                        else:
                            print("The input file %s does not contain template line: #var1, var2,..." % fileName)
                            return
                    numVariables = variables.__len__()
                    for i in range(startLine, lines.__len__()):
                        if lines[i][0] == '#':
                            continue
                        else:
                            variablesValues = lines[i].strip().replace(' ', '').split(',')
                            inputItem = dict()
                            for i in range(numVariables):
                                inputItem[variables[i]] = variablesValues[i]
                            result.append(inputItem)
                except:
                    print("Cannot load inputs!")
        except:
            print("Cannot open the input file: %s" % fileName)
        return result

    def exploit(self, parameters:dict={}) -> None:
        """
        Print potential vulnerabilities
        :param parameters: list of arguments to print
        :type parameters: dict
        :return: print potential vulnerabilities to screen
        :rtype: None
        """
        HTTPTester.STATUS += 1
        # self.STATUS += 1
        if parameters.__len__()>0:
            print("Potential vulnerability:", end='')
            for key in parameters:
                print(" %s (%s)" %(key,parameters[key]), end='')
            print('')
        else:
            print("The system has potential vulnerabilit(y/ies)!")

    def loadResponsePage(self, responsePage:str, responseCode:int=-1):
        if isinstance(responsePage, str) & isinstance(responseCode, int):
            # self.responsePage = responsePage
            self.responsePage.set(responsePage, responseCode=responseCode)

    def hasLoggedIn(self) -> bool:
        """
        :return: logged in or not
        :rtype: bool
        """
        return self.loggedIn

    def abort(self, message:str='') -> None:
        """
        :param message: message to print
        :type message: str
        :rtype: None
        """
        if (message != None) and (message.__len__() >0):
            print(message)
        # self.STATUS = 0

    def exit(self, message:str='') -> None:
        """

        :param message: message to print
        :type message: str
        :rtype: None
        """
        # if message != None and message.__len__()>0:
        #     print(message)
        # self.STATUS += 1
        pass

    def reachTheHighPredefinedThresholdAttempts(self) -> None:
        self.attemptCounter +=1
        # if "reachTheHighPredefinedThresholdAttempts" in self.configParams:
        #     if self.attemptCounter >= self.configParams["reachTheHighPredefinedThresholdAttempts"]:
        if "high_predefined_threshold_attempts" in self.complexInput:
            if self.attemptCounter >= self.complexInput["high_predefined_threshold_attempts"]:
                print("Attempt times:", self.attemptCounter)
                return True
        return False

    def resetAttemptCounter(self) -> None:
        self.attemptCounter = 0

    def updatePageParametersValues(self, fileName:str, sessionIDs:dict, parameters: dict):
        newSessionIDs = sessionIDs.copy()
        updateDict = dict()

        if fileName != "":
            try:
                fd = open(fileName, 'r')
                if fd:
                    # Load parameters in JSON format
                    try:
                        gottenParams = json.load(fd)
                        fd.close()

                        for key in gottenParams.keys():
                            # if key == "URL":
                            #     continue
                            # elif key == "Position":
                            #     continue
                            # elif key == "Others":
                            #     continue
                            # else:
                            #     updateDict[key] = gottenParams[key]
                            if not isinstance(gottenParams[key], dict):
                                if key=="URL":
                                    continue
                                elif key == "Position":
                                    continue
                                elif key == "Others":
                                    continue
                                else:
                                    updateDict[key] = gottenParams[key]
                            else:
                                for subKey in gottenParams[key].keys():
                                    updateDict[subKey] = gottenParams[key][subKey]
                    except:
                        print("Cannot load params in JSON format")
            except:
                print("Cannot open the login param file: %s" % fileName)

        for updateKey in updateDict.keys():
            for parameterKey in parameters.keys():
                if updateKey == parameterKey:
                    newSessionIDs[updateDict[updateKey]] = parameters[parameterKey]
        #Get keys and values from
        return newSessionIDs

    def runNetworkSniffingTool(self) -> None:
        self.loadProxy()
        self.usedProxy = self.getRandomProxy()

    def retrievePacketsFromNetworkSniffingTool(self) -> None:
        pageURL = HTTPTester.lastRequest.url
        lastMethod = HTTPTester.lastRequest.method
        capturedPacket = zapProxy.getLatestMessage(self.usedProxy, url=pageURL, method=lastMethod)
        # Generate HTTPHeaderTemplate based on captured packets
        self.generateHTTPTemplate(requestHeader=capturedPacket["requestHeader"],
                                  requestBody=capturedPacket["requestBody"])
        self.httpTemplate.clear()
        self.loadHTTPHeaderTemplate(self.httpHeaderTemplateFile)
        if "Cookie" in self.httpTemplate.keys():
            self.httpTemplate.pop("Cookie")



    def updateConfigFromLastRequest(self):
        # Set URL
        if HTTPTester.lastRequest.url != '':
            HTTPTester.curl.setopt(pycurl.URL, HTTPTester.lastRequest.url)

        #Set method and Post data (if having)
        method = HTTPTester.lastRequest.method
        if method != "" and method != None :
            if method == "GET":
                HTTPTester.curl.setopt(pycurl.HTTPGET, 1)
            elif method == "PUT":
                HTTPTester.curl.setopt(pycurl.PUT, 1)
            elif method == "HEAD":
                HTTPTester.curl.setopt(pycurl.NOBODY, 1)
            elif method == "POST":
                HTTPTester.curl.setopt(pycurl.POST, 1)
                # Add Post Data
                if len(HTTPTester.lastRequest.postData) > 0:
                    postData = HTTPTester.lastRequest.postData.copy()
                    encodedPostData = urlencode(postData)
                    HTTPTester.curl.setopt(pycurl.POSTFIELDS, encodedPostData)
        else:   # Method GET, by default
            HTTPTester.curl.setopt(pycurl.HTTPGET, 1)

        # Set HTTP version
        version = HTTPTester.lastRequest.httpVersion
        if version != "" and version != None:
            if version == "1.0":
                HTTPTester.curl.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_1_0)
            elif version == "1.1":
                HTTPTester.curl.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_1_1)
            elif version == "2.0":
                HTTPTester.curl.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_2_0)

        #Set headers
        if HTTPTester.lastRequest.headers.__len__() >0:
            HTTPTester.curl.setopt(pycurl.HTTPHEADER, HTTPTester.lastRequest.headers)

    def usedAllFuzzValues(self):
        if HTTPTester.fuzzContainer.hasNext() or HTTPTester.fuzzHTTPRequestContainer.hasNext():
            return False
        return True

    def useGetMethod(self) -> bool:
        if HTTPTester.lastRequest.getMethod().upper() == "GET":
            return True
        return False

    def alterHTTPRequestWithFuzzValues(self) -> None:
        """
        Set fuzz values to HTTP method, HTTP version, headers values
        :rtype: None
        """
        self.loadHTTPHeaderTemplate(self.httpHeaderTemplateFile)
        fuzzList = []

        for key in self.httpTemplate:
            if str(self.httpTemplate[key]).startswith("FUZZ"):
                fuzzList.append(self.httpTemplate[key])

        # check if the current fuzzContainer is NOT the correct case
        if not HTTPTester.fuzzHTTPRequestContainer.isTheCase(fuzzList):
            HTTPTester.fuzzHTTPRequestContainer.setFuzzList(fuzzList)  # set fuzz list
            HTTPTester.fuzzHTTPRequestContainer.loadFuzz(self.rootPath)  # load all fuzz values

        fuzzDictToUse = HTTPTester.fuzzHTTPRequestContainer.next()

        # check if the current fuzzContainer is NOT the correct case
        # if not HTTPTester.fuzzContainer.isTheCase(fuzzList):
        #     HTTPTester.fuzzContainer.setFuzzList(fuzzList)  # set fuzz list
        #     HTTPTester.fuzzContainer.loadFuzz(self.rootPath)  # load all fuzz values
        #
        # fuzzDictToUse = HTTPTester.fuzzContainer.next()

        if fuzzDictToUse != None:
            for key in self.httpTemplate:
                for fuzzKey in fuzzDictToUse:
                    if self.httpTemplate[key] == fuzzKey:
                        self.httpTemplate[key] = fuzzDictToUse[fuzzKey]
                        break