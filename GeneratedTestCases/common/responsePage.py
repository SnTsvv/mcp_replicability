import os
import json
import re

class ResponsePage:
    responseElements = ["HTTPVersion", "StatusCode", "StatusPhrase",
                        "Date", "Server", "Last-Modified", "Connection", "Cache-Control",
                        "Content-Length", "Content-Type", "Content-Language", "Content-Encoding",
                        "Content-Location", "Location", "Set-Cookie", "X-Frame-Options"]

    def __init__(self, path:str = "./", content:str ='', responseCode:int =-1, headers:list=[]) -> None:
        """

        :param content: contain of the response page
        :type content: str
        :param responseCode: HTTP response code (e.g., 200, 404...)
        :type responseCode: int
        :rtype: None
        """
        self.rootPath = path
        self.content = content
        self.code = responseCode
        self.headers = headers

    # def contain(self, message:str, rootpath:str= "./", usedRE:bool=False) -> bool:
    def contain(self, parameter: dict) -> bool:
        """
        :param parameter: parameter contains oracles to find
        :type parameter: dict
        :rtype: bool
        """
        usedRE: bool = True
        listOracles = []
        if isinstance(parameter, str):
            listOracles.append(parameter)
        elif isinstance(parameter, dict):
            for key in parameter.keys():
                # If key is NOT a HTTP response element
                if key not in ResponsePage.responseElements:
                    if isinstance(parameter[key], list):
                        for item in parameter[key]:
                            listOracles.append(item)      #(parameter[key][j] should be a str, int)
                    else:   # case of str, int
                        listOracles.append(parameter[key])
                # key IS a HTTP response element
                else:
                    pass
        elif isinstance(parameter, list):
            listOracles = parameter.copy()

        found = False
        for oracle in listOracles:
            # if str(self.content).find(oracle) >= 0:
            #     found = True
            #     break
            pattern = oracle
            p = re.compile(pattern)
            foundValues = p.findall(self.content)
            if foundValues.__len__() >= 1:
                found = True
                break

        return found

        """
        oracleFile = self.rootPath
        if isinstance(message, str):
            # oracleFile += "oracle-" + message.replace(' ','') + ".json";
            oracleFile += "oracle-" + message.replace(' ', '_') + ".json";
        else:
            return False
        #Load oracle file
        if os.path.exists(oracleFile) == False:
            return False
        oracle = dict()
        fd = open(oracleFile, 'r')
        if fd:
            oracle = json.load(fd)
        else:
            return False

        found = False
        # In case the test is based on the HTTP Response Code
        if oracle['Type'] == "RESPONSE_CODE":
            returnCode = self.code
            # Find if the returnCode exists in the oracle
            for i in range(len(oracle["Value"])):
                if str(returnCode).strip() == str(oracle["Value"][i]).strip():
                    found = True
                    break
            if (found == True and str(oracle["Comparison"]).strip() == "Fail") \
                    or (found == False and str(oracle["Comparison"]).strip() == "Pass"):
                return True
        elif oracle['Type'] == "IN-TEXT":
            for i in range(len(oracle["Value"])):
                if usedRE==False:
                    if str(self.content).find(oracle["Value"][i]) >= 0:
                        found = True
                        break
                else:
                    pattern = oracle["Value"][i]
                    p = re.compile(pattern)
                    foundValues = p.findall(self.content)
                    if foundValues.__len__() >= 1:
                        found = True
                        break
            if (found == True and str(oracle["Comparison"]).strip() == "Fail") \
                    or (found == False and str(oracle["Comparison"]).strip() == "Pass"):
                return True
        return False
        """

    def headerContain(self, header:str, value:str) -> bool:
        #check if there is a header naming "header", and "value"
        return False

    def set(self, content:str, responseCode:int=-1) -> None:
        """
        :param content: content of the response page
        :type content: str
        :param responseCode: HTTP response code
        :type responseCode: int
        :rtype: None
        """
        self.content = content
        if responseCode>0:
            self.code = responseCode

    def getContent(self) -> str:
        """
        :rtype: str
        """
        return self.content

    def getCode(self) -> int:
        """
        :rtype: int
        """
        return self.code

    def listOracle(parameter:dict) -> list:
        result = []
        # if isinstance(parameter, str):
        #     listOracles.append(parameter)
        # elif isinstance(parameter, dict):
        #     for key in parameter.keys():
        #         # If key is NOT a HTTP response element
        #         if key not in ResponsePage.responseElements:
        #             if isinstance(parameter[key], list):
        #                 for item in parameter[key]:
        #                     listOracles.append(item)      #(parameter[key][j] should be a str, int)
        #             else:   # case of str, int
        #                 listOracles.append(parameter[key])
        #         # key IS a HTTP response element
        #         else:
        #             pass
        # elif isinstance(parameter, list):
        #     listOracles = parameter.copy()
        return result