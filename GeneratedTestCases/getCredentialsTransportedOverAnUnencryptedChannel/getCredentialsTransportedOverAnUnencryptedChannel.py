import sys, datetime
from common.httpTester import HTTPTester
from common.system import System

class getCredentialsTransportedOverAnUnencryptedChannel(HTTPTester):
	def __init__(self, path="./", cookie=""):
		HTTPTester.__init__(self, path, cookie)
		self.loadComplexInput("complexInput.json")

	def run(self):
		print("Check getCredentialsTransportedOverAnUnencryptedChannel")
		print("---------------------------------")
		startTime = datetime.datetime.now()

		system = System(path=self.rootPath)
		maliciousUser = self
		input = self.complexInput

		parameters = dict()
		parameters["password"] = input["password"]
		parameters["username"] = input["username"]
		system.send("login page",parameters)
		maliciousUser.responsePage = system.responsePage
		parameter = dict()
		parameter["failed_login_message"] = input["failed_login_message"]
		if not maliciousUser.responsePage.contain(parameter) :
			parameters1 = dict()
			parameters1["protocol"] = input["protocol"]
			system.modify(True,False,parameters1)
			if not maliciousUser.useGetMethod() :
				system.resendModifiedPacket()
				parameter1 = dict()
				parameter1["failed_login_message"] = input["failed_login_message"]
				if not maliciousUser.responsePage.contain(parameter1) :
					maliciousUser.responsePage = system.responsePage
					parameters2 = dict()
					parameters2["protocol"] = input["protocol"]
					system.exploit(parameters2)
					maliciousUser.exit("The MALICIOUS user knows that the client’s account information is transferred on an unsecured channel")
				else:
					maliciousUser.abort("The MALICIOUS user cannot log in the system")
			else:
				parameters3 = dict()
				maliciousUser.exploit(parameters3)
				maliciousUser.abort("The credentials has been sent to the server using GET method")
		else:
			maliciousUser.abort("The MALICIOUS user filled wrong account credential or accessed wrong login page")

		print("---------------------------------")
		endTime = datetime.datetime.now()
		print("Tested time:", endTime - startTime)

#----- Running code: getCredentialsTransportedOverAnUnencryptedChannel -----#
path = "./"
if sys.argv.__len__()>2:
	path = sys.argv[1]

threads = []
getCredentialsTransportedOverAnUnencryptedChannelTester = getCredentialsTransportedOverAnUnencryptedChannel(path)
getCredentialsTransportedOverAnUnencryptedChannelTester.start()
threads.append(getCredentialsTransportedOverAnUnencryptedChannelTester)
# Wait for all threads to finish #
for thread in threads:
	thread.join()
sys.exit(getCredentialsTransportedOverAnUnencryptedChannelTester.STATUS)
