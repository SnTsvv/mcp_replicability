import sys, datetime
from common.httpTester import HTTPTester
from common.system import System

class guessUserAccount(HTTPTester):
	def __init__(self, path="./", cookie=""):
		HTTPTester.__init__(self, path, cookie)
		self.loadComplexInput("complexInput.json")

	def run(self):
		print("Check guessUserAccount")
		print("---------------------------------")
		startTime = datetime.datetime.now()

		system = System(path=self.rootPath)
		maliciousUser = self
		input = self.complexInput

		combinationIter = input["combination"].__iter__()
		while True: 
			try: 
				combination = combinationIter.__next__()
				parameters = dict()
				parameters["password"] = combination["password"]
				parameters["username"] = combination["username"]
				system.send("login page",parameters)
				maliciousUser.responsePage = system.responsePage
				parameter = dict()
				parameter["wrong_password_message"] = combination["wrong_password_message"]
				if not maliciousUser.responsePage.contain(parameter) :
					parameter1 = dict()
					parameter1["unknown_combination_message"] = combination["unknown_combination_message"]
					if not maliciousUser.responsePage.contain(parameter1) :
						parameters1 = dict()
						parameters1["password"] = combination["password"]
						parameters1["username"] = combination["username"]
						system.exploit(parameters1)
					else:
						maliciousUser.abort("The MALICIOUS user does not know whether the username exists or not")
				else:
					parameters2 = dict()
					parameters2["username"] = combination["username"]
					system.exploit(parameters2)
			except StopIteration: 
				break
		maliciousUser.exit("The MALICIOUS tried all the username and password combinations")

		print("---------------------------------")
		endTime = datetime.datetime.now()
		print("Tested time:", endTime - startTime)

#----- Running code: guessUserAccount -----#
path = "./"
if sys.argv.__len__()>2:
	path = sys.argv[1]

threads = []
guessUserAccountTester = guessUserAccount(path)
guessUserAccountTester.start()
threads.append(guessUserAccountTester)
# Wait for all threads to finish #
for thread in threads:
	thread.join()
sys.exit(guessUserAccountTester.STATUS)
