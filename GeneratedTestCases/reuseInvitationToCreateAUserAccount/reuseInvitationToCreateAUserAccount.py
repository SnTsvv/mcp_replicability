import sys, datetime
from common.httpTester import HTTPTester
from common.system import System

class reuseInvitationToCreateAUserAccount(HTTPTester):
	def __init__(self, path="./", cookie=""):
		HTTPTester.__init__(self, path, cookie)
		self.loadComplexInput("complexInput.json")

	def run(self):
		print("Check reuseInvitationToCreateAUserAccount")
		print("---------------------------------")
		startTime = datetime.datetime.now()

		system = System(path=self.rootPath)
		maliciousUser = self
		input = self.complexInput

		parameters = dict()
		parameters["password"] = input["password"]
		parameters["username"] = input["username"]
		system.send("login page",parameters)
		maliciousUser.runNetworkSniffingTool()
		parameters1 = dict()
		parameters1["invitation_request"] = input["invitation_request"]
		system.send("invitation page",parameters1)
		maliciousUser.retrievePacketsFromNetworkSniffingTool()
		parameters2 = dict()
		parameters2["email"] = input["email"]
		parameters2["message"] = input["message"]
		parameters2["recipient"] = input["recipient"]
		system.modify(False,True,parameters2)
		system.resendModifiedPacket()
		maliciousUser.responsePage = system.responsePage
		parameter = dict()
		parameter["failure_message"] = input["failure_message"]
		if not maliciousUser.responsePage.contain(parameter) :
			parameters3 = dict()
			maliciousUser.exploit(parameters3)
			maliciousUser.exit("The MALICIOUS user successfully creates a new account on the EDLAH2 system")
		else:
			maliciousUser.abort("The MALICIOUS cannot create a new account")

		print("---------------------------------")
		endTime = datetime.datetime.now()
		print("Tested time:", endTime - startTime)

#----- Running code: reuseInvitationToCreateAUserAccount -----#
path = "./"
if sys.argv.__len__()>2:
	path = sys.argv[1]

threads = []
reuseInvitationToCreateAUserAccountTester = reuseInvitationToCreateAUserAccount(path)
reuseInvitationToCreateAUserAccountTester.start()
threads.append(reuseInvitationToCreateAUserAccountTester)
# Wait for all threads to finish #
for thread in threads:
	thread.join()
sys.exit(reuseInvitationToCreateAUserAccountTester.STATUS)
