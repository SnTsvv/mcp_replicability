import sys, datetime
from common.httpTester import HTTPTester
from common.system import System

class bypassAuthorizationSchema(HTTPTester):
	def __init__(self, path="./", cookie=""):
		HTTPTester.__init__(self, path, cookie)
		self.loadComplexInput("complexInput.json")

	def run(self):
		print("Check bypassAuthorizationSchema")
		print("---------------------------------")
		startTime = datetime.datetime.now()

		system = System(path=self.rootPath)
		maliciousUser = self
		input = self.complexInput

		roleIter = input["role"].__iter__()
		while True: 
			try: 
				role = roleIter.__next__()
				parameters = dict()
				parameters["password"] = role["password"]
				parameters["username"] = role["username"]
				system.send("login page",parameters)
				parameter = dict()
				parameter["authentication_failure_message"] = role["authentication_failure_message"]
				if not maliciousUser.responsePage.contain(parameter) :
					resourceIter = role["resource"].__iter__()
					while True: 
						try: 
							resource = resourceIter.__next__()
							if not eval(resource["the_resource_contains_the_role_parameter_in_the_URL"]) :
								if not eval(resource["the_resource_contains_the_role_parameter_in_the_HTTP_post_data"]) :
									system.request(resource)
									maliciousUser.responsePage = system.responsePage
									parameter1 = dict()
									parameter1["error_message"] = resource["error_message"]
									if not maliciousUser.responsePage.contain(parameter1) :
										parameters1 = dict()
										parameters1["resource"] = resource["resource"]
										parameters1["role"] = role["role"]
										system.exploit(parameters1)
									else:
										maliciousUser.abort("The MALICIOUS user CANNOT execute a function dedicated to another user with different role")
								else:
									parameters2 = dict()
									parameters2["role_values"] = resource["role_values"]
									system.modify(False,True,parameters2)
									system.request(resource)
									maliciousUser.responsePage = system.responsePage
									parameter2 = dict()
									parameter2["error_message"] = resource["error_message"]
									if not maliciousUser.responsePage.contain(parameter2) :
										parameters3 = dict()
										parameters3["resource"] = resource["resource"]
										parameters3["role"] = role["role"]
										system.exploit(parameters3)
									else:
										maliciousUser.abort("The MALICIOUS user CANNOT execute a function dedicated to another user with different role")
							else:
								parameters4 = dict()
								parameters4["role_values"] = resource["role_values"]
								system.modify(True,False,parameters4)
								system.request(resource)
								maliciousUser.responsePage = system.responsePage
								parameter3 = dict()
								parameter3["error_message"] = resource["error_message"]
								if not maliciousUser.responsePage.contain(parameter3) :
									parameters5 = dict()
									parameters5["resource"] = resource["resource"]
									parameters5["role"] = role["role"]
									system.exploit(parameters5)
								else:
									maliciousUser.abort("The MALICIOUS user CANNOT execute a function dedicated to another user with different role")
						except StopIteration: 
							break
					maliciousUser.exit("The MALICIOUS user has executed a function dedicated to another user with different role")
				else:
					maliciousUser.abort("The MALICIOUS user CANNOT login")
			except StopIteration: 
				break

		print("---------------------------------")
		endTime = datetime.datetime.now()
		print("Tested time:", endTime - startTime)

#----- Running code: bypassAuthorizationSchema -----#
path = "./"
if sys.argv.__len__()>2:
	path = sys.argv[1]

threads = []
bypassAuthorizationSchemaTester = bypassAuthorizationSchema(path)
bypassAuthorizationSchemaTester.start()
threads.append(bypassAuthorizationSchemaTester)
# Wait for all threads to finish #
for thread in threads:
	thread.join()
sys.exit(bypassAuthorizationSchemaTester.STATUS)
