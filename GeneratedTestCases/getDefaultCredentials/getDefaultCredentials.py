import sys, datetime
from common.httpTester import HTTPTester
from common.system import System

class getDefaultCredentials(HTTPTester):
	def __init__(self, path="./", cookie=""):
		HTTPTester.__init__(self, path, cookie)
		self.loadComplexInput("complexInput.json")
		UsernamePasswordFile = self.rootPath + "UsernamePassword.txt"
		self.UsernamePassword = self.loadInputFromFile(UsernamePasswordFile)
		self.UsernamePasswordIter = self.UsernamePassword.__iter__()

	def run(self):
		print("Check getDefaultCredentials")
		print("---------------------------------")
		startTime = datetime.datetime.now()

		system = System(path=self.rootPath)
		maliciousUser = self
		input = self.complexInput

		while True:
			try:
				UsernamePassword = self.UsernamePasswordIter.__next__()
				usernamepasswordDictionary = dict()
				usernamepasswordDictionary["username"] = UsernamePassword["username"]
				usernamepasswordDictionary["password"] = UsernamePassword["password"]
				system.send("login page",usernamepasswordDictionary)
				maliciousUser.responsePage = system.responsePage
				parameter = dict()
				parameter["timeout_message"] = input["timeout_message"]
				if not maliciousUser.responsePage.contain(parameter) :
					if not maliciousUser.reachTheHighPredefinedThresholdAttempts() :
						if system.hasLoggedIn() :
							break
					else:
						maliciousUser.abort("The malicious user has not been logged into the system")
				else:
					maliciousUser.abort("The MALICIOUS user has not been logged into the system")
			except StopIteration:
				break
		parameters = dict()
		maliciousUser.exploit(parameters)
		maliciousUser.exit("The MALICIOUS user has been logged into the system by using a default credential")

		print("---------------------------------")
		endTime = datetime.datetime.now()
		print("Tested time:", endTime - startTime)

#----- Running code: getDefaultCredentials -----#
path = "./"
if sys.argv.__len__()>2:
	path = sys.argv[1]

threads = []
getDefaultCredentialsTester = getDefaultCredentials(path)
getDefaultCredentialsTester.start()
threads.append(getDefaultCredentialsTester)
# Wait for all threads to finish #
for thread in threads:
	thread.join()
sys.exit(getDefaultCredentialsTester.STATUS)
