package snt.oclsolver.strategies;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.uml2.uml.Property;

import snt.oclsolver.datatypes.PrimitiveTuple;
import snt.oclsolver.datatypes.PrimitiveValueTuple;
import snt.oclsolver.distance.ClassDiagramTestData;
import snt.oclsolver.distance.ClassifierAttributeInQueryOperations;
import snt.oclsolver.distance.NestedBooleanOperatorParser;
import snt.oclsolver.distance.PrimitiveAttributeInQueryConstructs;
import snt.oclsolver.distance.Problem;
import snt.oclsolver.distance.QueryType;
import snt.oclsolver.distancecalculators.IntegerDistanceCalculator;
import snt.oclsolver.ocloperations.BooleanOperation;
import snt.oclsolver.ocloperations.NumericOperation;
import snt.oclsolver.ocloperations.OCLOperation;
import snt.oclsolver.ocloperations.ObjectBooleanOperation;
import snt.oclsolver.queryparser.QueryParser;
import snt.oclsolver.tuples.ClassifierTuple;
import snt.oclsolver.tuples.ClassifierValueTuple;
import snt.oclsolver.tuples.ValueTuple;

public class ObjectDistanceCalc {
	private String query = null;
	private boolean hasSizeOperation;
	private static HashMap<String, ArrayList> objsWithDistances = new HashMap<String, ArrayList>();
	private static ArrayList<NumericOperation> numericOps /*= new ArrayList<NumericOperation>()*/;
	public void setHasSizeOperation(boolean hasSO) {
		this.hasSizeOperation = hasSO;
	}

	public ObjectDistanceCalc(String query1) {
		this.query = query1;
		numericOps = new ArrayList<NumericOperation>();
	}

	public ArrayList<Double> getObjectsdistance(
			ArrayList<ClassifierValueTuple> cvTuples, Property prop,
			ClassifierValueTuple cvtParam, String opName) {
		ArrayList<Double> result = new ArrayList<Double>();
		ArrayList<Double> temp = new ArrayList<Double>();
		double distancePos = 0.0;
		double distanceNeg = 0.0;
		for (ClassifierValueTuple cvt : cvTuples) {
			temp.clear();
			//numericOps.clear();
			temp = getSingleObjectdistance(cvt, prop, cvtParam, opName);
			distancePos += temp.get(0);
			distanceNeg += temp.get(1);
		}
		result.add(distancePos);
		result.add(distanceNeg);
		return result;
	}

	public ArrayList<Double> getSingleObjectdistance(ClassifierValueTuple cvt,
			Property prop, ClassifierValueTuple cvtParam, String opName) {

		double distancePos = 0.0;
		double distanceNeg = 0.0;
		double disPos = 0.0;
		double disNeg = 0.0;
		ClassifierValueTuple rltdCvt = null;
		ArrayList<Double> result = new ArrayList<Double>();
		ArrayList<Double> temp = new ArrayList<Double>();
		ArrayList<ValueTuple> vTuples = cvt.getAttributeValues();
		if (cvtParam != null) {
			rltdCvt = cvtParam;
		} else {
			rltdCvt = cvt;
		}
//		ArrayList<QueryType> operators = QueryParser
//				.getOPERATORSInQuerypart(query);
		ArrayList<QueryType> operators = Problem.queryWithOperators.get(query);
		if (cvt.getObjOps() != null
				&& cvt.getObjOps().getBooleanOpList().size() > 0
				&& cvt.getObjOps().getTraversedForDistance() == false) {
			for (ObjectBooleanOperation obop : cvt.getObjOps()
					.getBooleanOpList()) {

				disPos = (Double) obop.getClassifierDistance(null,
						obop.getNeg(), rltdCvt, "", "");
				disNeg = (Double) obop.getClassifierDistance(null,
						!obop.getNeg(), rltdCvt, "", "");
				if (opName.equalsIgnoreCase("forall")) {
					boolean collectionChk = false;
					for (QueryType operator : operators) {
						if (operator != operators.get(0)) {
							if (operator == QueryType.SelectAll
									|| operator == QueryType.collect) {
								collectionChk = true;
								break;
							} else
								break;
						}
					}
					if (!collectionChk) {
						distancePos += disPos;
						distanceNeg += disNeg;
					}

				} else {
					if (disPos == 0.0) {
						distancePos += disPos;
						distanceNeg += disNeg;

					}
				}
				if (disPos == 0.0) {
					//numericOps.clear();
					temp = getValuesDistance(vTuples, prop, rltdCvt);
					double res1 = (IntegerDistanceCalculator.normalize(temp
							.get(0)));
					double res2 = (IntegerDistanceCalculator.normalize(temp
							.get(1)));
					distancePos += res1;
					distanceNeg += res2;

				}
			}
		} else {
			//numericOps.clear();
			temp = getValuesDistance(vTuples, prop, rltdCvt);
			distancePos = temp.get(0);
			distanceNeg = temp.get(1);
		}

		result.add(distancePos);
		result.add(distanceNeg);
		return result;

	}

	@SuppressWarnings("unchecked")
	public ArrayList<Double> getValuesDistance(ArrayList<ValueTuple> vTuples,
			Property prop, ClassifierValueTuple rltdCvt) {
		double distancePos = 0.0;
		double distanceNeg = 0.0;
		double disPos = 0.0;
		double disNeg = 0.0;

		double valTupDisPos=0.0;
		double valTupDisNeg=0.0;
		Object preResPos = null;
		Object preResNeg = null;
		boolean isTraversedForDistance = false;
//		ArrayList<NumericOperation> numericOps = new ArrayList<NumericOperation>();
		ArrayList<Double> result = new ArrayList<Double>();
		ArrayList<ValueTuple> tempVTuples = new ArrayList<ValueTuple>();
		tempVTuples.addAll(vTuples);

		for (ValueTuple vt : tempVTuples) {
			int index = vTuples.indexOf(vt);
			if (index != -1) {
				ValueTuple tempVt = vTuples.get(index);
				if (vt instanceof PrimitiveValueTuple) {
					PrimitiveValueTuple it = (PrimitiveValueTuple) tempVt;
					PrimitiveTuple pt = (PrimitiveTuple) it
							.getRelatedProperty();
					if (prop == null
							|| pt.getRoleeName().equalsIgnoreCase(
									prop.getName())) {
						ArrayList<PrimitiveAttributeInQueryConstructs> aiqc = pt
								.getAttributeInQueryConstructs();
						for (PrimitiveAttributeInQueryConstructs ac : aiqc) {
							if (tempVt.getTraversedForDistance() == false) {
								for (OCLOperation oclOp : ac
										.getPrecedenceArray()) {
									boolean isTraversed = false,targetConNumOp = false;
									boolean constantIsString = false,noNumericOp = false;
									if (oclOp instanceof BooleanOperation) {
										BooleanOperation bop = (BooleanOperation) oclOp;
										if (bop.getConstant() instanceof String) {
											constantIsString = true;
											String targetCons = ""
													+ bop.getConstant();
											if (targetCons.contains("+")
													|| targetCons.contains("-")
													|| targetCons.contains("*")
													|| targetCons.contains("/")
													|| targetCons.contains("%")) {	
												targetConNumOp = true;
												if(numericOps.isEmpty())
													noNumericOp = true;
												
												/**
												 * TODO: Change 'objsWithDistances' key and update values 
												 * 		 structure stored against the key. 
												 * */
												if(objsWithDistances.containsKey(rltdCvt.getObjectName())){
													ArrayList<Double> distances = objsWithDistances.get(rltdCvt.getObjectName());

													if(!distances.isEmpty()){
														preResPos = distances.get(0);
														preResNeg = distances.get(1);
													}
												}
												else{
													if(!rltdCvt.getrltdInstances().isEmpty()){
														for(ClassifierValueTuple cvt : rltdCvt.getrltdInstances()){
															if(objsWithDistances.containsKey(cvt.getObjectName())){
																ArrayList<Double> distances  = objsWithDistances.get(cvt.getObjectName());
																if(!distances.isEmpty()){
																	preResPos = distances.get(0);
																	preResNeg = distances.get(1);
																}
																//break;
																disPos = oclOp.getPrimitiveDistance(it,
																		ac.getNeg(), cvt, preResPos);
																distancePos += disPos;
																valTupDisPos += disPos;
																disNeg = oclOp.getPrimitiveDistance(it,
																		!ac.getNeg(), cvt, preResNeg);
																isTraversed = true;
																distanceNeg += disNeg;
																valTupDisNeg += disNeg;
															}
														}
													}
												}
//												for (NumericOperation no : numericOps) {
//													String targetName;
//													if (no.getNumericOpConstant() instanceof Property) {
//														Property propCons = (Property) no
//																.getNumericOpConstant();
//														targetName = propCons
//																.getName();
//													} else if (no
//															.getNumericOpConstant() instanceof ArrayList<?>) {
//														ArrayList<Property> propList = (ArrayList<Property>) no
//																.getNumericOpConstant();
//														targetName = propList
//																.get(propList
//																		.size() - 1)
//																		.getName();
//													} else {
//														targetName = ""
//																+ no.getNumericOpConstant();
//													}
//													if (targetCons
//															.contains(targetName) && !isPreResFilled) {
//														preResPos = no
//																.getResultPos();
//														preResNeg = no
//																.getResultNeg();
//													}
//													//isTraversed = true;
//												}
											}
										}
									}
									
									//for string having expression a+b/a-b
									if(constantIsString && !noNumericOp){
										//for both numeric operations
										if(preResPos != null && targetConNumOp)
											disPos = oclOp.getPrimitiveDistance(it,
													ac.getNeg(), rltdCvt, preResPos);
										//for string operations that don't contains numeric operation
										else if(preResPos == null && !targetConNumOp)
											disPos = oclOp.getPrimitiveDistance(it,
													ac.getNeg(), rltdCvt, preResPos);
										distancePos += disPos;
										valTupDisPos += disPos;
										if(preResNeg != null)
											disNeg = oclOp.getPrimitiveDistance(it,
													!ac.getNeg(), rltdCvt, preResNeg);
										else if(preResNeg == null && !targetConNumOp)
											disNeg = oclOp.getPrimitiveDistance(it,
													!ac.getNeg(), rltdCvt, preResNeg);
										isTraversed = true;
									}
									//for other types of ocl op
									else if(!constantIsString){
										disPos = oclOp.getPrimitiveDistance(it,
												ac.getNeg(), rltdCvt, preResPos);
										distancePos += disPos;
										valTupDisPos += disPos;
										disNeg = oclOp.getPrimitiveDistance(it,
												!ac.getNeg(), rltdCvt, preResNeg);
										isTraversed = true;
									}
									distanceNeg += disNeg;
									valTupDisNeg += disNeg;
									//if both distances are 0 then make isTraversedForDistance true
									if(disPos == 0.0 && disNeg == 0.0 && isTraversed)
										isTraversedForDistance = true;
									
									
									if (oclOp instanceof NumericOperation) {
										ArrayList<Double> distances = new ArrayList<Double>();
										distances.add(0,disPos);
										distances.add(1,disNeg);
										if(!objsWithDistances.containsKey(rltdCvt.getObjectName()))
											objsWithDistances.put(rltdCvt.getObjectName(), distances);
										else{
											objsWithDistances.get(rltdCvt.getObjectName()).clear();
											objsWithDistances.get(rltdCvt.getObjectName()).addAll(distances);
										}
										
										NumericOperation nop = (NumericOperation) oclOp;
										NumericOperation newNop = new NumericOperation(nop.getNumericOpertaion(), nop.getNumericOpConstant(),null,null);
										newNop.setResultPos(disPos);
										newNop.setResultNeg(disNeg);
										ac.setNumericOp(newNop);
										if(!numericOps.contains(newNop)){
											newNop.setResultPos(disPos);
											newNop.setResultNeg(disNeg);
											numericOps.add(newNop);
										}
									}
									preResPos = null;
									preResNeg = null;
								}

							}

						}
					}
				} else {
					boolean chkRltObj = false;
					ClassifierValueTuple it = (ClassifierValueTuple) tempVt;
					ClassifierTuple ctInner = (ClassifierTuple) it
							.getRelatedProperty();
					if (prop == null
							|| ctInner.getRoleeName().equalsIgnoreCase(
									prop.getName())) {
						ArrayList<ClassifierAttributeInQueryOperations> aiqc = ctInner
								.getClassifierAttributeInQueryConstructs();
						ArrayList<ClassifierValueTuple> targetObjs = new ArrayList<ClassifierValueTuple>();
						for (ClassifierValueTuple cvtInner : ctInner
								.getObjectTuples()) {
							if (cvtInner.getrltdInstances().contains(rltdCvt)) {
								targetObjs.add(cvtInner);
								chkRltObj = true;
								break;
							}
						}
						if (chkRltObj == false) {
							ClassifierValueTuple ctTarget=null;
							if (ctInner.getObjectTuples().size() == 0) {
								ArrayList<ClassifierTuple> rltdTuples = ClassDiagramTestData
										.getInstance().getClassifier(
												ctInner.getClassName());
								for (ClassifierTuple rct : rltdTuples) {
									if (rct.getObjectTuples().size() > 0) {
										ctTarget = rct.getObjectTuples().get(0);
									}
								}
							} else {
								 ctTarget =ctInner.getObjectTuples().get(0);
							}
							targetObjs.add(ctTarget);
							if(!ctTarget.getrltdInstances().contains(rltdCvt) /*&& rltdCvt.getrltdInstances().contains(ctTarget)*/)
								ctTarget.addRltObj(rltdCvt);
							/*targetObjs.add(ctInner.getObjectTuples().get(0));
							ctInner.getObjectTuples().get(0).addRltObj(rltdCvt);*/
						}
						for (ClassifierValueTuple cvTup : targetObjs) {
							for (ValueTuple vTup : cvTup.getAttributeValues()) {
								vTup.setTraversedForDistance(false);
								vTup.setIsSatisfied(false);
							}
						}
						for (ClassifierAttributeInQueryOperations aiq : aiqc) {
							if (tempVt.getTraversedForDistance() == false) {
								ArrayList<OCLOperation> precedenceArray = aiq
										.getPrecedenceArray();
								Object ans1 = targetObjs;
								for (OCLOperation oclOp : precedenceArray) {
									ans1 = oclOp.getClassifierDistance(ans1,
											aiq.getNeg(), rltdCvt, "", "");
									//isTraversedForDistance = true;
								}
								ArrayList<Object> kk = new ArrayList<Object>();
								// ArrayList<Double> k = new ArrayList<Double>();
								// ArrayList<Double> k1 = new ArrayList<Double>();
								if (ans1 instanceof Double) {
									double res1 = (Double) ans1;
									valTupDisPos = res1;
									distancePos += res1;
									distanceNeg = 0;
								} else if (ans1 instanceof ArrayList<?>) {
									kk = (ArrayList<Object>) ans1;
									// if (kk.get(0) instanceof ArrayList<?>
									// && kk.get(0) instanceof ArrayList<?>) {
									// k = (ArrayList<Double>) kk.get(0);
									// k1 = (ArrayList<Double>) kk.get(1);
									// distancePos = distancePos + k.get(0);
									// distanceNeg = distanceNeg + k1.get(0);
									// }
									if (kk.get(0) instanceof Double
											&& kk.get(0) instanceof Double) {
										distancePos = distancePos
												+ (Double) kk.get(0);
										valTupDisPos = (Double) kk.get(0);
										distanceNeg = distanceNeg
												+ (Double) kk.get(1);
										valTupDisNeg = (Double) kk.get(1);
									}
								}
								if(precedenceArray==null || precedenceArray.size()==0)
								{
									//ArrayList<Double>distances = calculatePrimitiveValueDistance(targetObjs.get(0).getAttributeValues(), null, targetObjs.get(0));
									ArrayList<Double>distances = getValuesDistance(targetObjs.get(0).getAttributeValues(), null, rltdCvt);
									distancePos += distances.get(0);
									distanceNeg += distances.get(1);
								}
							}
						}
					}
				}
				if (isTraversedForDistance)
					tempVt.setTraversedForDistance(true);
				if (valTupDisPos == 0.0 && hasSizeOperation) {
					tempVt.setIsSatisfied(true);
				}
				if (valTupDisNeg == 0.0 && hasSizeOperation) {
					tempVt.setIsSatisfied(true);
				}
				valTupDisPos = 0.0;
				valTupDisNeg = 0.0;
			}
		}
		
		result.add(distancePos);
		result.add(distanceNeg);
		return result;
	}
	
	public ArrayList<Object> getValuesDistanceWithNestdBooleanOps(
			ArrayList<ValueTuple> vTuples, Property prop,
			ClassifierValueTuple cvt) {
		ArrayList<Object> result = new ArrayList<Object>();
		ArrayList<Double> P1distance = new ArrayList<Double>();
		ArrayList<Double> P1distance_neg = new ArrayList<Double>();
		ArrayList<String> vtRoleNameArray = new ArrayList<String>();
		int count = 0;
		int classifierTypeCount = 0;
		String tempQuery = query;
		NestedBooleanOperatorParser NestedBOpParObj = new NestedBooleanOperatorParser();
		ArrayList<ValueTuple> tempVTuples = new ArrayList<ValueTuple>();
		tempVTuples.addAll(vTuples);
		for (ValueTuple vt : tempVTuples) {
			// count++;
			int index = vTuples.indexOf(vt);
			if (index != -1) {
				ValueTuple tempVt = vTuples.get(index);
				if (tempVt instanceof PrimitiveValueTuple) {
					String primitive = tempVt.getRelatedProperty().getRoleeName();
					PrimitiveValueTuple it = (PrimitiveValueTuple) tempVt;
					PrimitiveTuple pt = (PrimitiveTuple) it.getRelatedProperty();

					ArrayList<PrimitiveAttributeInQueryConstructs> aiqc = pt
							.getAttributeInQueryConstructs();

					for (int z = 0; z < aiqc.size(); z++) {
						if (tempVt.getTraversedForDistance() == false) {
							ArrayList<OCLOperation> precedenceArray = aiqc.get(z)
									.getPrecedenceArray();

							for (OCLOperation oclOp : precedenceArray) {
								boolean constantIsString = false;
								if (oclOp instanceof BooleanOperation) {
									BooleanOperation bop = (BooleanOperation) oclOp;
									if (bop.getConstant() instanceof String) {
										constantIsString = true;
									}
								}
								if (!constantIsString) {
									P1distance.add(oclOp
											.getPrimitiveDistance(it,aiqc.get(z).getNeg(), cvt,null));
									P1distance_neg
											.add(oclOp.getPrimitiveDistance(it,!aiqc.get(z).getNeg(), cvt,null));
								}
							}
							// if(!P1distance.isEmpty()){
							// res1D.add(P1distance.get(0));
							// res1D.add(P1distance_neg.get(0));
							// }
						}

					}

				}

				else {
					count = 0;
					String classifier = tempVt.getRelatedProperty().getRoleeName();
					String vtString = tempVt.getRelatedProperty().getRoleeName();
					vtRoleNameArray.add(vtString);
					NestedBooleanOperatorParser n = new NestedBooleanOperatorParser();
					String part = n.getQueryPart(query, vtString, vtRoleNameArray,
							count);

					boolean chkRltObj = false;
					ClassifierValueTuple it = (ClassifierValueTuple) tempVt;
					ClassifierTuple ctInner = (ClassifierTuple) it
							.getRelatedProperty();
					ArrayList<ClassifierAttributeInQueryOperations> aiqc = ctInner
							.getClassifierAttributeInQueryConstructs();
					ArrayList<ClassifierValueTuple> targetObjs = new ArrayList<ClassifierValueTuple>();
					for (ClassifierValueTuple cvtInner : ctInner.getObjectTuples()) {
						if (cvtInner.getrltdInstances().contains(cvt)) {
							targetObjs.add(cvtInner);
							chkRltObj = true;
							break;
						}
					}
					if (chkRltObj == false) {
						ClassifierValueTuple ctTarget=null;
						if (ctInner.getObjectTuples().size() == 0) {
							ArrayList<ClassifierTuple> rltdTuples = ClassDiagramTestData
									.getInstance().getClassifier(
											ctInner.getClassName());
							for (ClassifierTuple rct : rltdTuples) {
								if (rct.getObjectTuples().size() > 0) {
									ctTarget = rct.getObjectTuples().get(0);
								}
							}
						} else {
							 ctTarget =ctInner.getObjectTuples().get(0);
						}
						targetObjs.add(ctTarget);
						if(!ctTarget.getrltdInstances().contains(cvt) /*&& cvt.getrltdInstances().contains(ctTarget)*/)
							ctTarget.addRltObj(cvt);
					}
					Object ans;
					for (ClassifierAttributeInQueryOperations aiq : aiqc) {
						count++;
						for (ClassifierValueTuple cvTup : targetObjs) {
							for (ValueTuple vTup : cvTup.getAttributeValues()) {
								vTup.setTraversedForDistance(false);
								vTup.setIsSatisfied(false);
							}
						}
						ArrayList<ClassifierValueTuple> temp = new ArrayList<ClassifierValueTuple>();
						double disPos = 0.0;
						double disNeg = 0.0;
						int countObjType = -1;
						for (ClassifierValueTuple obj : targetObjs) {

							ArrayList<ValueTuple> objVTuples = obj
									.getAttributeValues();

							for (ValueTuple objVt : objVTuples) {

								if (objVt instanceof PrimitiveValueTuple) {
								} else {
									classifierTypeCount++;
									break;
								}
							}
						}
						// ********
						if (!targetObjs.isEmpty()
								&& targetObjs.get(0).getObjOps() != null
								&& targetObjs.get(0).getObjOps().getBooleanOpList()
								.size() > 0
								&& targetObjs.get(0).getObjOps()
								.getTraversedForDistance() == false) {
							for (ClassifierValueTuple cvtTar : targetObjs) {
								disPos = 0.0;
								disNeg = 0.0;
								countObjType = -1;
								for (ObjectBooleanOperation obop : cvtTar
										.getObjOps().getBooleanOpList()) {

									disPos = (Double) obop.getClassifierDistance(
											null, obop.getNeg(), cvt, "", "");
									disNeg = (Double) obop.getClassifierDistance(
											null, !obop.getNeg(), cvt, "", "");
									P1distance.add(disPos);
									P1distance_neg.add(disNeg);
								}
								ArrayList<Double> res = new ArrayList<Double>();
								if (P1distance.size() > 1) {
									countObjType++;
									res = NestedBOpParObj.queryWithBracket(
											P1distance, P1distance_neg, part,
											classifierTypeCount, countObjType);
								}
								// if (disPos == 0.0)
								if (res.size() != 0) {
									if (res.get(0) == 0.0
											|| P1distance.get(0) == 0.0) {
										temp.add(cvtTar);
									}
								} else {
									if (P1distance.get(0) == 0.0) {
										temp.add(cvtTar);
									}
								}

							}
							targetObjs.clear();
							targetObjs=temp;
							targetObjs.get(0).getObjOps().setTraversedForDistance(true);
						}
						ans = targetObjs;
						// *****
						if (tempVt.getTraversedForDistance() == false) {
							ArrayList<OCLOperation> precedenceArray = aiq
									.getPrecedenceArray();

							for (OCLOperation oclOp : precedenceArray) {

								if (count == 1) {

									ans = oclOp.getClassifierDistance(ans,
											aiq.getNeg(), cvt, part, query);
								}

								else {
									part = n.getQueryPart(query, vtString,
											vtRoleNameArray, count);
									// ans = oclOp.getClassifierDistance(ans,
									// aiq.getNeg(), cvt, part, query);
									ans = oclOp.getClassifierDistance(ans,
											aiq.getNeg(), cvt, part, tempQuery);
								}
							}
							if(precedenceArray==null || precedenceArray.size()==0)
							{
								ArrayList<Double>distances = getValuesDistance(targetObjs.get(0).getAttributeValues(), null, cvt);
								double distancePos = distances.get(0);
								double distanceNeg = distances.get(1);
								P1distance.add(distancePos);
								P1distance_neg.add(distanceNeg);
							}
							//tempVt.setTraversedForDistance(true);
						}
						if (ans instanceof Double) {
							P1distance.add((Double) ans);
							P1distance_neg.add((double) 0);

						} else if (ans instanceof ArrayList<?>) {
							ArrayList<?> res11 = (ArrayList<?>) ans;
							if (res11.get(0) instanceof Double) {
								P1distance.add((Double) res11.get(0));
								P1distance_neg.add((Double) res11.get(1));
							}
						}
					}
				}
			}
		}
		result.add(P1distance);
		result.add(P1distance_neg);
		return result;
	}
}
