import inspect
import dis
import threading

from common.system import System
from common.httpTester import HTTPTester
from common.resource import Resource
from common.resource import URL
from common.resource import PostData
from common.role import Role
from common.responsePage import ResponsePage
from common.requestInfo import RequestInfo
# from common.mcpTestCase import MCPTestCase
from common.fuzzContainer import FuzzContainer

def getAllAttributes(object):
    result = dict()
    for key in object.__dict__:
        result[key] = str(type(object.__dict__[key]))[8:-2]
    return result

def getClassAttribute(classType) -> dict:
    # container of attribute (key is attribute name, value is type of attribute)
    attributes = dict()
    #1. Get static attributes
    allAttributes = getAllAttributes(classType)
    if allAttributes.__len__() > 0:
        for att in allAttributes:
            if isinstance(allAttributes[att], str):
                if (allAttributes[att] != "function" and allAttributes[att] != "NoneType"):
                    attributes[att] = allAttributes[att]

    #2. Get attributes which are initiated by __init__
    defaultType = 'str'
    # get instructions
    codeLines = dis.Bytecode(classType.__init__).dis()

    #separate disCode in blocks
    blocks = list()
    tempBlock = list()
    for eachLine in codeLines.split("\n"):
        if eachLine == "":
            blocks.append(tempBlock)
            tempBlock = list()
            continue

        while "  " in eachLine:
            eachLine = eachLine.replace("  ", " ")
        eachLine = eachLine[1:]

        #delete line number and code number
        tempList = str(eachLine).split(" ")
        while not str(tempList[0][0]).isupper():
            tempList.pop(0)

        # print(tempList)
        tempBlock.append(tempList)
    #delete 2 last lines in the last block (related to return value)
    if len(blocks) > 0:
        lastBlock = blocks.pop(len(blocks) - 1)
        lastBlock.pop(len(lastBlock) - 1)
        lastBlock.pop(len(lastBlock) - 1)
        blocks.append(lastBlock)

    #process each block to get attribute
    for block in blocks:
        blockLen = len(block)
        if block[0][0] == "LOAD_GLOBAL":
            # recursively get attribute of
            callSuperClass = False
            lineNum = 1
            while block[lineNum][0] == "LOAD_ATTR":
                if block[lineNum][2][1:-1] == "__init__":
                    callSuperClass = True
                    break
                else:
                    lineNum += 1
            if callSuperClass:
                if str(block[blockLen-2][0]).startswith("CALL_FUNCTION"):
                    if block[blockLen-1][0] == "POP_TOP":
                        superClass = block[0][2][1:-1]
                        code = block[1][0]
                        add = block[1][2]
                        i = 2
                        while code == "LOAD_ATTR" and (not add == "(__init__)"):
                            superClass += "." + add[1:-1]
                            code = block[i][0]
                            add = block[i][2]
                            i += 1
                        attributes.update(getClassAttribute(eval(superClass)))
            elif block[blockLen-2][0] == "LOAD_FAST" and block[blockLen-2][2] == "(self)":
                if block[blockLen-1][0] == "STORE_ATTR":
                    type = block[0][2][1:-1]
                    i = 1
                    while block[i][0] == "LOAD_ATTR":
                        type += "." + block[i][2][1:-1]
                        i += 1
                    attributes[str(block[blockLen-1][2])[1:-1]] = type

        elif block[0][0] == "LOAD_CONST":
            if block[blockLen-2][0] == "LOAD_FAST" and block[blockLen-2][2] == "(self)":
                if block[blockLen-1][0] == "STORE_ATTR":
                    type = "str"
                    if str(block[0][2])[1:-1].isdigit():
                        type = "int"
                    elif str(block[0][2])[1:-1] == "False" or str(block[0][2])[1:-1] == "True":
                        type = "bool"
                    attributes[str(block[blockLen-1][2])[1:-1]] = type

        elif block[0][0] == "LOAD_FAST":
            if block[blockLen-2][0] == "LOAD_FAST" and block[blockLen-2][2] == "(self)":
                if block[blockLen-1][0] == "STORE_ATTR":
                    attributes[str(block[blockLen - 1][2])[1:-1]] = defaultType

        elif block[0][0] == "BUILD_LIST":
            if block[blockLen-2][0] == "LOAD_FAST" and block[blockLen-2][2] == "(self)":
                if block[blockLen-1][0] == "STORE_ATTR":
                    attributes[str(block[blockLen-1][2])[1:-1]] = "list"

    # print(attributes)
    return attributes


def print_class(instance: object, classAttribute:bool = True, method:bool = True, rtype:bool = True) -> str:
    """
    :type instance: object
    """

    if str(instance).startswith("<class") == False:
        print("The current instance %s is not a class" %str(instance))
        return

    result = ""

    # Class name
    className = str(instance).replace("<class \'", '').replace("\'>", '')
    result += "Class: " + className + "\n"

    lastDotIndex = className.rfind(".")
    classShortName = className
    if lastDotIndex>=0:
        classShortName = className[lastDotIndex+1:]

    # Attributes
    if classAttribute:
        classAttribute = getClassAttribute(instance)
        for key in classAttribute:
            attrStr = str("\tAttribute:\t%s\t%s" % (classAttribute[key], key))
            result += attrStr + "\n"

    # Methods
    if method:

        allAttributes = dir(instance)
        if allAttributes.__len__() > 0:
            for att in allAttributes:
                if inspect.isfunction(eval(classShortName + "." +att)):
                    methodName = str(att)
                    result += "\tMethod:\t" + methodName + "\n"

                    fullMethod = str(classShortName+ "." + methodName)
                    methodArgs = inspect.getfullargspec(eval(fullMethod))
                    annotations = methodArgs.annotations
                    listArgs = methodArgs.args
                    if "self" in listArgs:
                        listArgs.remove('self')

                    for arg in listArgs:
                        argStr = ""
                        if arg in annotations:
                            argStr = str("\t\tParameter:\t%s\t%s" % (
                            str(annotations[arg]).replace("<class \'", '').replace("\'>", ''), arg))
                        else:
                            argStr = str("\t\tParameter:\t\t%s" % arg)
                        result += argStr + "\n"

                    # Print Variable arguments (type, name)
                    if methodArgs.varargs != None:
                        argsSpecStr = ""
                        if methodArgs.varargs in annotations:
                            argsSpecStr = str("\t\tVariable arguments:\t%s\t%s" % (
                            str(annotations[methodArgs.varargs]).replace("<class \'", '').replace("\'>", ''),
                            methodArgs.varargs))
                        else:
                            argsSpecStr = str("\t\tVariable arguments:\t\t%s" % methodArgs.varargs)
                        result += argsSpecStr + "\n"

                    # Print return type
                    if rtype & ('return' in annotations):
                        rtypeStr = str(
                            "\t\tReturn:\t%s" % str(annotations['return']).
                            replace("<class \'", '').replace("\'>",''))
                        result += rtypeStr + "\n"

    return result

def print_classes_info(*args, method:bool = True, rtype:bool = True):
    result = ""
    for arg in args:
        result += print_class(arg, method=method, rtype=rtype) + "\n"
    return result

def exportAPI(fileName:str = "API.txt") -> None:

    apiStr = print_classes_info( HTTPTester, System, RequestInfo, Resource, Role, URL, PostData, ResponsePage, FuzzContainer)

    try:
        f = open(fileName, "w")
        if f:
            f.write(apiStr)
            f.close()
    except:
        print("Cannot open file:", fileName)

    print(apiStr)

exportAPI(fileName="API.txt")