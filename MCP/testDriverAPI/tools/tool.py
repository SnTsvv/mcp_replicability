import random, string


def randomGenString(length:int) -> str:
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(length))

def eraseArticle(message:str) -> str:
    result = message
    while result.lower().startswith("the ") | result.lower().startswith("a ") | result.lower().startswith("an "):
        if result.lower().startswith("the "):
            result = result[4:].strip()
        elif result.lower().startswith("a "):
            result = result[2:].strip()
        elif result.lower().startswith("an "):
            result = result[3:].strip()
    return result