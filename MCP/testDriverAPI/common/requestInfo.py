class RequestInfo:
    def __init__(self):
        self.url = ""  # URL
        self.method = ""  # HTTP method (GET, POST, HEAD...)
        self.postData = dict()  # Post Data in the case uing POST Method
        self.headers = []  # list of HTTP headers
        self.httpVersion = "1.1"  # HTTP Version (1.0, 1.1, 2.0)

    def update(self):
        if self.method != "POST":
            self.postData = dict()

    def deleteCookie(self):
        found = False
        index = -1
        for i in range(self.headers.__len__()):
            if str(self.headers[i]).startswith("Cookie: "):
                found = True
                index = i
                break
        if found:
            self.headers.pop(index)

    def getProtocol(self) -> str:
        if self.url.__len__() == 0:
            return ""
        index = self.url.index("://")
        if index < 1:
            return ""
        prot = self.url[0:index]
        return prot.upper()

    def getMethod(self) -> str:
        return self.method

    def getHeaderValue(self, header: str) -> str:
        """

        :rtype: str
        """
        if self.headers.__len__() < 1:
            return None
        for h in self.headers:
            if str(h).startswith(header + ":"):
                value = str(h)[header.__len__() + 1:].strip()
                return value
        return None

    def headerContain(self, header: str, value: str) -> bool:
        getValue = self.getHeaderValue(header)

        if getValue == None:
            return False
        return value in getValue
