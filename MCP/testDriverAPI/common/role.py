class Role:
    def __init__(self, parameters: dict = None) -> None:
        """
        :param parameters: parameters to set Role (name, username, password)
        :type parameters:dict
        :rtype: None
        """
        self.attributes = dict()
        if isinstance(parameters, dict):
            self.attributes = parameters.copy()
            if "name" not in parameters.keys():
                self.attributes["name"] = ''
            if "username" not in parameters.keys():
                self.attributes["username"] = ''
            if "password" not in parameters.keys():
                self.attributes["password"] = ''
        else:
            self.attributes["name"] = ''
            self.attributes["username"] = ''
            self.attributes["password"] = ''

    def getName(self) -> str:
        """
        :rtype: str
        """
        return self.attributes["name"]

    def setName(self, name: str) -> None:
        """
        :param name: name value
        :type name: str
        :rtype: None
        """
        self.attributes["name"] = name

    def getUsername(self) -> str:
        """
        :rtype: str
        """
        return self.attributes["username"]

    def setUsername(self, username:str) -> None:
        """
        :param username: username value
        :type username: str
        :rtype: None
        """
        self.attributes["username"] = username

    def getPassword(self) -> str:
        """
        :rtype: str
        """
        return self.attributes["password"]

    def setPassword(self, password:str) -> None:
        """
        :param password: password value
        :type password: str
        :rtype: None
        """
        self.attributes["password"] = password

    def getAttribute(self, attribute:str) -> object:
        """
        :param attribute: attribute name
        :type attribute: str
        :rtype: object
        """
        if isinstance(attribute, str):
            if attribute in self.attributes.keys():
                return self.attributes[attribute]
        return None

    def get(self, attribute:str) -> object:
        """
        :param attribute: attribute name
        :type attribute: str
        :rtype: object
        """
        if isinstance(attribute, str):
            if attribute in self.attributes.keys():
                return self.attributes[attribute]
        return None

    def setAttribute(self, attribute:dict) -> None:
        """
        :param attribute: attribute to set
        :type attribute: dict
        :rtype: None
        """
        if isinstance(attribute, dict):
            self.attributes.update(attribute)