import urllib.parse as urlparse
from urllib.parse import urlsplit, urlencode, urlunparse

class Resource:
    # def __init__(self, url="", postData=None):
    #     self.attributes = dict()
    #     self.attributes['url'] = URL('abc.com/path/?p=xyz')
    #     self.url = URL(url)
    #     self.postData = PostData(postData)
    def __init__(self, resource:dict =None) -> None:
        """
        :param resource: information about resource including url, post data
        :type resource: dict
        :rtype: None
        """
        self.attributes = dict()
        if isinstance(resource, dict):
            self.attributes = resource.copy()
            if "url" not in resource.keys():
                if "resource" in resource.keys():
                    self.attributes["url"] = URL(url=resource["resource"])
                else:
                    self.attributes["url"] = URL()
            if "postData" not in resource.keys():
                self.attributes["postData"] = PostData()
        else:
            self.attributes["url"] = URL()
            self.attributes["postData"] = PostData()

    def get(self, attribute:str) -> object:
        """
        :param attribute: attribute name
        :type attribute: str
        :return: the object of attribute
        :rtype: object
        """
        if isinstance(attribute, str):
            if attribute in self.attributes.keys():
                return self.attributes[attribute]
        return None

    def set(self, attribute: str, parameters: dict) -> None:
        """
        :param attribute: name of the attribute
        :param parameters: parameters of the atribute to set
        """
        if isinstance(attribute, str):
            if isinstance(parameters, dict):
                if attribute== 'url':
                    self.attributes['url'].setParams(parameters)
                else:
                    self.attributes[attribute].update(parameters)

class URL:
    def __init__(self, url='', parameters:dict =None) -> None:
        """
        :param url: URL
        :type url: str
        :param parameters: parameters in the URL
        :type parameters: dict
        :rtype: None
        """
        self.url = ""
        if isinstance(url, str):
            self.url = url
        # else:
        #     self.url = ""
        if parameters != None:
            if isinstance(parameters, dict):
                self.setParams(parameters)

    def getURL(self) -> str:
        """
        :rtype: str
        """
        return self.url

    def setURL(self, url:str) -> None:
        """
        :param url: URL to set
        :type url: str
        :rtype: None
        """
        if isinstance(url, str):
            self.url = url

    def getParams(self) -> dict:
        """
        :rtype: dict
        """
        url = self.url
        urlParts = list(urlparse.urlparse(url))
        urlQuery = dict(urlparse.parse_qsl(urlParts[4]))
        return urlQuery

    def setParams(self, parameters:dict) -> bool:
        """
        :param parameters: parameters to set
        :type parameters: dict
        :rtype: bool
        """
        setParam = False  # return value
        if isinstance(parameters, dict):
            url = self.url
            # Update URL with params
            if len(parameters) > 0 & url.strip()!= '':
                # Get current queries in the URL
                urlParts = list(urlparse.urlparse(url))
                urlQuery = dict(urlparse.parse_qsl(urlParts[4]))
                addParams = dict()
                for param in parameters:
                    addParams[param] = parameters[param]
                    urlQuery.update(addParams)

                urlParts[4] = urlencode(urlQuery)
                self.url = urlunparse(urlParts)
                setParam = True
        return setParam

    def containParameter(self, parameter:str) -> bool:
        """
        :param parameter: parameter name to find
        :type parameter: str
        :rtype: bool
        """
        if isinstance(parameter, str):
            url = self.url
            urlParts = list(urlparse.urlparse(url))
            urlQuery = dict(urlparse.parse_qsl(urlParts[4]))
            if parameter in urlQuery:
                return True
        return False

class PostData:
    def __init__(self, postData:dict =None) -> None:
        """
        :param postData: data to put in POST data
        :type postData: dict
        :rtype: None
        """
        self.postData = dict()
        if isinstance(postData, dict):
            self.postData.update(postData)

    def containParameter(self, parameter:str) -> bool:
        """
        :param parameter: parameter name
        :type parameter: str
        :rtype: bool
        """
        if isinstance(parameter, str):
            if parameter in self.postData.keys():
                return True
        return False

    def isnull(self) -> bool:
        """
        Check if the postData is null
        :rtype: bool
        """
        if self.postData.__len__()==0:
            return True
        return False

# r1 = Resource(url="http://abc.com/subpath/?q=app")
# print(r1.attributes['url'].containsParameter('abc'))
# if r1.url.containsParameter("abc"):
#     print("Have")